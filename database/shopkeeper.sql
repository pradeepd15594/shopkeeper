-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2016 at 09:37 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shopkeeper`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
`id` bigint(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `address`, `date_added`) VALUES
(25, 'pp', '1234567890', 'vmvnjn', '2016-01-27 17:06:34'),
(24, 'vimal', '7805936353', 'ramnager', '2016-01-18 11:00:31'),
(18, 'kamal', '7805936356', 'india', '2016-01-15 21:02:22'),
(19, 'ajeet', '99876555677', 'bhilai', '2015-12-17 18:12:00'),
(20, 'Test', '9876932431', 'abc', '2016-01-15 20:10:40'),
(21, 'Test', '9876932431', 'abc', '2016-01-15 20:10:49'),
(22, 'Testvfdfd', '9876932431', 'abccdfgfd', '2016-01-15 20:21:12'),
(23, 'Testvfdf', '9876932431', 'abcvff', '2016-01-15 20:21:33'),
(26, '0', '0', '0', '2016-06-15 15:19:56'),
(27, '250', '0', '2563.', '2016-06-15 16:57:19'),
(28, '250', '0', '2563.', '2016-06-15 16:59:16'),
(29, '250', '0', '2563.', '2016-06-15 17:01:04'),
(30, 'Ajeet sir', '0', 'hhjyuj', '2016-06-15 17:07:19'),
(31, 'Ajeet sir', '0', 'hhjyuj', '2016-06-15 17:07:41'),
(32, 'gthy', '0', '2222222222222222222', '2016-06-15 17:22:58');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
`id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Agents table' AUTO_INCREMENT=50 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `address`, `contact_no`, `created_on`, `updated_on`) VALUES
(28, 'NIDHI GUPTA', 'PANCH RASTA SUPELA BHILAI', '8821039583', '2016-01-03 07:03:39', '2016-01-03 07:03:39'),
(33, 'MAHAVIR SINGH', 'CONTRECTOR COLONYN SUPELA BHILAI DIST DURG (C.G.)', '0000000000', '2016-01-03 07:08:47', '2016-01-03 07:08:47'),
(34, 'TARA CHAND SAHU', 'RAMNAGAR MUKTIDHAM SUPELA BHILAI DIST DURG (C.G.)', '0000000000', '2016-01-03 07:09:23', '2016-01-03 07:09:23'),
(40, 'SURAJ KUMAR', 'DUNDERA BHILAI DIST DURG (C.G.)', '0000000000', '2016-01-03 07:12:58', '2016-01-03 07:12:58'),
(41, 'GUPTA SERVICES', 'C/O VIJAY GUPTA SECTOR 6 BHILAI DIST DURG (C.G.)', '0000000000', '2016-01-03 07:14:00', '2016-01-03 07:14:00'),
(42, 'VJAY GUPTA (PASSPORT)', 'SHANTI NAGAR SUPELA BHILAI DIST DURG (C.G.)', '0000000000', '2016-01-03 07:14:46', '2016-01-03 07:14:46'),
(49, 'RAHUL (GANPATI MOTOR)', 'GANPATI MOTOR G.E. ROAD SUPELA BHILAI DIST DURG (C.G.)', '0000000000', '2016-01-03 07:26:15', '2016-01-03 07:26:15');

-- --------------------------------------------------------

--
-- Table structure for table `member_account`
--

CREATE TABLE IF NOT EXISTS `member_account` (
`id` bigint(255) NOT NULL,
  `member_id` bigint(255) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `remark` text NOT NULL,
  `purpose` enum('debit','credit') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `member_account`
--

INSERT INTO `member_account` (`id`, `member_id`, `amount`, `remark`, `purpose`, `created_at`, `updated_at`) VALUES
(62, 33, '2000.00', '2000.00', 'debit', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 33, '2000.00', '2000.00', 'credit', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 28, '25.00', '25 credit', 'credit', '2016-06-15 14:20:17', '2016-06-15 14:20:17'),
(84, 28, '1800.00', 'Amount due for15062016098727', 'debit', '2016-06-15 14:22:24', '2016-06-15 14:22:24'),
(85, 28, '1000.00', 'Amount due for15062016098727', 'credit', '2016-06-15 14:22:25', '2016-06-15 14:22:25'),
(86, 28, '500.00', '500', 'credit', '2016-06-15 14:25:31', '2016-06-15 14:25:31'),
(87, 28, '2500.00', 'Amount due for-15062016092671', 'credit', '2016-06-15 14:28:12', '2016-06-15 14:28:12');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` bigint(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `cost` decimal(20,2) NOT NULL,
  `total_price` decimal(20,2) NOT NULL COMMENT 'cost*quantity',
  `quantity` bigint(255) NOT NULL,
  `avilable` bigint(255) NOT NULL COMMENT 'stock-avilable',
  `reorder_point` bigint(255) NOT NULL DEFAULT '0',
  `user_id` bigint(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `product_code`, `cost`, `total_price`, `quantity`, `avilable`, `reorder_point`, `user_id`, `created_at`, `updated_at`) VALUES
(110, 'Sumsung Mobile', '025', '100.00', '10000.00', 47, 50, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'Sumsung Freez', '0369', '458.00', '6944.00', 48, 25, 0, 0, '2016-05-28 13:21:52', '2016-05-28 13:21:52'),
(112, 'LAVA', '025', '25.00', '84520.00', 47, 281, 0, 0, '2016-05-28 13:23:09', '2016-05-28 13:23:09'),
(113, 'Nokia', '025', '458.00', '700.00', 34, 52, 0, 0, '2016-05-28 13:24:22', '2016-05-28 13:24:22'),
(114, 'Microsoft', '02', '52.00', '952.00', 22, 3695, 0, 0, '2016-05-28 13:25:31', '2016-05-28 13:25:31'),
(115, 'Microsoft 123', '05324882', '52.00', '10.00', 5, 25, 25, 0, '2016-05-28 13:25:44', '2016-05-28 13:25:44'),
(116, '4586', '4586', '458.00', '2100388.00', 4585, 0, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
`id` bigint(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `category`, `created_at`) VALUES
(1, '255555', '2016-06-15 00:00:00'),
(3, 'hjuyjuyj', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE IF NOT EXISTS `purchase_order` (
`id` bigint(255) NOT NULL,
  `invoice_id` bigint(255) NOT NULL,
  `invoice_type` varchar(255) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `customer_phone` bigint(255) NOT NULL,
  `address` text NOT NULL,
  `tax` bigint(255) NOT NULL,
  `discount` bigint(255) NOT NULL,
  `paidby` varchar(255) NOT NULL,
  `pro_count` int(20) NOT NULL,
  `total_amt` varchar(255) NOT NULL,
  `payable_amt` varchar(255) NOT NULL COMMENT 'Total Payable Amount',
  `paid` bigint(255) NOT NULL COMMENT 'Total Paid Amount',
  `ret_change` varchar(255) NOT NULL,
  `cc_no_val` varchar(255) NOT NULL,
  `cc_holder_val` varchar(255) NOT NULL,
  `cheque` varchar(255) NOT NULL,
  `hold_ref` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `cheque_no` varchar(255) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `invoice_id`, `invoice_type`, `customer`, `customer_phone`, `address`, `tax`, `discount`, `paidby`, `pro_count`, `total_amt`, `payable_amt`, `paid`, `ret_change`, `cc_no_val`, `cc_holder_val`, `cheque`, `hold_ref`, `status`, `cheque_no`, `date`) VALUES
(5, 2092015084922, 'retail', 'rajesh', 0, '', 0, 0, 'cash', 1, '10531.13', '12210.01', 0, '-12210.01', '', '', '', '', 0, '1', '2015-09-02 18:14:03'),
(6, 2092015030417, 'retail', 'raju', 0, '', 0, 0, 'cash', 2, '24743.00', '24743.00', 24743, '0.00', '', '', '', '', 0, '1', '2015-09-02 18:20:28'),
(7, 2092015021362, 'wholesale', 'hem raj', 0, '', 0, 0, 'cash', 1, '26892.00', '26892.00', 26800, '-92.00', '', '', '', '', 1, '1', '2015-09-02 18:53:46'),
(8, 7092015053944, 'wholesale', 'ramesh', 0, '', 0, 0, 'cash', 1, '50220.00', '50220.00', 50220, '0.00', '', '', '', '', 0, '1', '2015-09-07 14:37:22'),
(9, 18092015003310, 'retail', 'lksn', 0, '', 246, 0, 'cash', 3, '4389.45', '5266.14', 0, '-5266.14', '', '', '', '', 0, '1', '2015-09-18 13:09:09'),
(11, 23102015047341, 'retail', 'Test', 0, '', 133, 0, 'cash', 2, '2289.45', '2765.05', 2800, '34.95', '', '', '', '', 0, '1', '2015-10-23 13:12:34'),
(13, 28102015068676, 'retail', 'Test', 0, '', 189, 0, 'cash', 2, '3264.96', '3941.10', 4000, '58.90', '', '', '', '', 0, '0', '2015-10-28 18:01:29'),
(14, 6112015001824, 'retail', 'Test', 0, '', 169, 0, 'cash', 2, '2949.45', '3551.10', 3552, '0.90', '', '', '', '', 0, '0', '2015-11-03 15:55:25'),
(15, 6112015059550, 'retail', 'kamal', 0, '', 169, 178, 'cash', 2, '2949.45', '3373.54', 3374, '0.46', '', '', '', '', 0, '0', '2015-11-06 22:56:59'),
(16, 4122015057652, 'retail', 'kamal', 0, '', 246, 0, 'cash', 3, '4114.41', '4991.10', 5000, '8.90', '', '', '', '', 0, '0', '2015-12-04 12:13:10'),
(17, 17122015095308, 'wholesale', 'ajeet', 0, '', 112, 105, 'cash', 2, '1698.90', '1995.00', 2000, '5.00', '', '', '', '', 1, '0', '2015-12-17 18:12:00'),
(30, 1, 'bjhb', 'bv g', 0, '', 55, 55, '55', 55, '58', '55', 5, '588', '88', '', '', '5', 0, '0', '2016-01-21 00:00:00'),
(33, 15062016035346, 'retail', '250', 255, '2563.', 0, 0, 'cash', 0, '', '535', 600, '', '', '', '', '', 0, '', '2016-06-15 17:01:04'),
(34, 15062016003302, 'retail', 'Ajeet sir', 452, 'hhjyuj', 0, 0, 'cash', 0, '', '5710', 5710, '', '', '', '', '', 0, '', '2016-06-15 17:07:19'),
(35, 15062016063872, 'retail', 'Ajeet sir', 452, 'hhjyuj', 0, 0, 'cash', 0, '', '5710', 5710, '', '', '', '', '', 0, '', '2016-06-15 17:07:41'),
(36, 15062016015365, 'retail', 'gthy', 2558682222, '2222222222222222222', 0, 0, 'cash', 0, '', '1526', 2000, '', '', '', '', '', 0, '', '2016-06-15 17:22:58');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_product`
--

CREATE TABLE IF NOT EXISTS `purchase_product` (
`id` bigint(255) NOT NULL,
  `invoice_id` bigint(255) NOT NULL,
  `product_id` bigint(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `qty` int(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `total_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `hold_ref` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `is_cancelled` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `purchase_product`
--

INSERT INTO `purchase_product` (`id`, `invoice_id`, `product_id`, `customer_name`, `product_name`, `qty`, `price`, `created_at`, `total_amount`, `hold_ref`, `status`, `is_cancelled`) VALUES
(32, 15062016035346, 112, '250', '', 1, '25.00', '2016-06-15 17:01:04', '25.00', '', 0, 0),
(33, 15062016035346, 114, '250', '', 1, '52.00', '2016-06-15 17:01:04', '52.00', '', 0, 0),
(34, 15062016063872, 110, 'Ajeet sir', '', 52, '100.00', '2016-06-15 17:07:41', '5200.00', '', 0, 0),
(35, 15062016063872, 111, 'Ajeet sir', '', 1, '458.00', '2016-06-15 17:07:41', '458.00', '', 0, 0),
(36, 15062016063872, 114, 'Ajeet sir', '', 1, '52.00', '2016-06-15 17:07:41', '52.00', '', 0, 0),
(37, 15062016015365, 113, 'gthy', '', 1, '458.00', '2016-06-15 17:22:58', '458.00', '', 0, 0),
(38, 15062016015365, 114, 'gthy', '', 1, '52.00', '2016-06-15 17:22:58', '52.00', '', 0, 0),
(39, 15062016015365, 110, 'gthy', '', 1, '100.00', '2016-06-15 17:22:58', '100.00', '', 0, 0),
(40, 15062016015365, 111, 'gthy', '', 1, '458.00', '2016-06-15 17:22:58', '458.00', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
`id` bigint(255) NOT NULL,
  `store_name` varchar(255) NOT NULL,
  `shop_no` varchar(255) NOT NULL,
  `tel_no` bigint(255) NOT NULL,
  `address` text NOT NULL,
  `tax_include` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `store_name`, `shop_no`, `tel_no`, `address`, `tax_include`, `created_at`, `updated_at`) VALUES
(1, 'ghhj', 'hnjhmjm', 155, 'zcvffrhytj', '2535vfg', '2016-06-15 15:19:51', '2016-06-15 15:19:51'),
(2, 'ghhj', 'hnjhmjm', 155, 'zcvffrhytj', '2535vfg', '2016-06-15 14:48:50', '2016-06-15 14:48:50');

-- --------------------------------------------------------

--
-- Table structure for table `stock_control`
--

CREATE TABLE IF NOT EXISTS `stock_control` (
`id` bigint(255) NOT NULL,
  `supl_id` bigint(255) NOT NULL,
  `supl_invoice` varchar(255) NOT NULL,
  `product_id` bigint(255) NOT NULL,
  `payable_amt` bigint(255) NOT NULL,
  `paid_amt` bigint(255) NOT NULL,
  `balance_amt` bigint(255) NOT NULL,
  `pay_mode` varchar(255) NOT NULL,
  `neft_tr_id` varchar(255) DEFAULT NULL,
  `rtgs_tr_id` varchar(255) DEFAULT NULL,
  `cheque_no` varchar(255) DEFAULT NULL,
  `order_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `is_close` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stock_control`
--

INSERT INTO `stock_control` (`id`, `supl_id`, `supl_invoice`, `product_id`, `payable_amt`, `paid_amt`, `balance_amt`, `pay_mode`, `neft_tr_id`, `rtgs_tr_id`, `cheque_no`, `order_date`, `due_date`, `is_close`) VALUES
(1, 28, '45u8lo', 0, 1800, 3500, -1700, 'Cash', '', '', '', '2016-06-15 14:22:24', '2016-06-15 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `stock_product`
--

CREATE TABLE IF NOT EXISTS `stock_product` (
`sp_id` bigint(255) NOT NULL,
  `product_description` bigint(255) NOT NULL,
  `qty` bigint(255) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `order_no` bigint(255) NOT NULL,
  `batch_no` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `stock_product`
--

INSERT INTO `stock_product` (`sp_id`, `product_description`, `qty`, `price`, `amount`, `order_no`, `batch_no`, `created_at`, `updated_at`) VALUES
(1, 110, 25, '36.00', '900.00', 1, '', '2016-06-15 14:22:24', '2016-06-15 14:22:24'),
(2, 112, 25, '36.00', '900.00', 1, '', '2016-06-15 14:22:24', '2016-06-15 14:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` bigint(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `user_name`, `password`, `user_type`, `mobile`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', '7805936353', 'bgfnhgjuyj', '2016-06-11 17:15:55', '2016-06-11 17:15:55'),
(3, 'staff', 'staff', '123', 'staff', '7805936353', 'I am Staff of this Product. ', '2016-06-15 19:13:25', '2016-06-15 19:13:25'),
(4, 'bhai', 'bhai', 'bhai', 'admin', '7805936353', 'hgh,tu,522566', '2016-06-11 17:14:25', '2016-06-11 17:14:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_account`
--
ALTER TABLE `member_account`
 ADD PRIMARY KEY (`id`), ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_product`
--
ALTER TABLE `purchase_product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_control`
--
ALTER TABLE `stock_control`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_product`
--
ALTER TABLE `stock_product`
 ADD PRIMARY KEY (`sp_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `member_account`
--
ALTER TABLE `member_account`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `purchase_product`
--
ALTER TABLE `purchase_product`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stock_control`
--
ALTER TABLE `stock_control`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stock_product`
--
ALTER TABLE `stock_product`
MODIFY `sp_id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `member_account`
--
ALTER TABLE `member_account`
ADD CONSTRAINT `member_account_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
