<!DOCTYPE html>
<!-- saved from url=(0076)http://localhost/new_pos(5-02-2016)/bill-print?id=04062016076750&type=retail -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <title>Invoice No &lt;div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"&gt;

&lt;h4&gt;A PHP Error was encountered&lt;/h4&gt;

&lt;p&gt;Severity: Notice&lt;/p&gt;
&lt;p&gt;Message:  Undefined variable: invoice_id&lt;/p&gt;
&lt;p&gt;Filename: stuff/bill_print.php&lt;/p&gt;
&lt;p&gt;Line Number: 5&lt;/p&gt;

&lt;/div&gt; | &lt;div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;"&gt;

&lt;h4&gt;A PHP Error was encountered&lt;/h4&gt;

&lt;p&gt;Severity: Notice&lt;/p&gt;
&lt;p&gt;Message:  Undefined variable: sitename&lt;/p&gt;
&lt;p&gt;Filename: stuff/bill_print.php&lt;/p&gt;
&lt;p&gt;Line Number: 5&lt;/p&gt;

&lt;/div&gt;</title>        	
    <style type="text/css" media="all">
        body {
            /*max-width: 800px;*/
            margin: 0 auto;
            text-align: center;
            color: #000;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;       
			background: #e3e6e7;
			padding-bottom: 20px;
		}
		#bill {
			background: #ffffff;
			width: 678px;
			margin: 0 auto;
			margin-top: 20px;
			display: block;
			border: 1px solid #c4c7c7;
			padding: 15px 15px 15px 15px;
			position: relative;
			z-index: 0;
		}		
        #wrapper {
            min-width: 250px;
            margin: 0 auto;
			margin-bottom:-20px;
        }        
        #wrapper img {
            max-width: 300px;
            width: auto;
        }
		a, a:hover{text-decoration:none;}
        
		.barcode{margin-top: -3px; padding: 0 236px; letter-spacing: 7.5px;}
		
        h2, h3, p {margin: 5px 0;}        
        .left {
            width: 60%;
            float: left;
            text-align: left;
            margin-bottom: 3px;
        }        
        .right {
            width: 40%;
            float: right;
            text-align: right;
            margin-bottom: 3px;
        }        
        .table,
        .totals {
            width: 100%;
            margin: 10px 0;
        }        
        .table th {border-bottom: 1px solid #000;}        
        .table td {padding: 0;}        
        .totals td {width: 24%; padding: 0;}        
        .table td:nth-child(2){overflow: hidden;}
        .btn {
			display: inline-block;
			margin-bottom: 0;
			font-weight: normal;
			text-align: center;
			vertical-align: middle;
			cursor: pointer;
			background-image: none;
			border: 1px solid transparent;
			white-space: nowrap;
			padding: 6px 12px;
			font-size: 14px;
			line-height: 1.428571429;
			border-radius: 0px;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}
		.btn:focus,
		.btn:active:focus,
		.btn.active:focus {
			outline: thin dotted;
			outline: 5px auto -webkit-focus-ring-color;
			outline-offset: -2px;
		}
		.btn:hover,
		.btn:focus {
			color: #333333;
			text-decoration: none;
		}
		.btn:active,
		.btn.active {
			outline: 0;
			background-image: none;
			-webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
			box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
		}
		.btn-primary {
			color: #ffffff;
			background-color: #428bca;
			border-color: #357ebd;
		}
		.btn-primary:hover,
		.btn-primary:focus,
		.btn-primary:active,
		.btn-primary.active{
			color: #ffffff;
			background-color: #3071a9;
			border-color: #285e8e;
		}
		
		.btn-success {
			color: #ffffff;
			background-color: #2ecc71;
			border-color: #29b765;
		}
		.btn-success:hover,
		.btn-success:focus,
		.btn-success:active,
		.btn-success.active{
			color: #ffffff;
			background-color: #25a25a;
			border-color: #1e854a;
		}
		.btn-info {
			color: #ffffff;
			background-color: #3498db;
			border-color: #258cd1;
		}
		.btn-info:hover,
		.btn-info:focus,
		.btn-info:active,
		.btn-info.active{
			color: #ffffff;
			background-color: #217dbb;
			border-color: #1c699d;
		}
		
		.btn-danger {
			color: #ffffff;
			background-color: #e74c3c;
			border-color: #e43725;
		}
		.btn-danger:hover,
		.btn-danger:focus,
		.btn-danger:active,
		.btn-danger.active {
			color: #ffffff;
			background-color: #d62c1a;
			border-color: #b62516;
		}
		.clearfix{clear:both;}
		
        @media print {
            body {text-transform: uppercase;padding-bottom: 0px;}
			#bill {margin-top: 0px;}
            #buttons {display: none;}
            #wrapper {width: 100%;margin: 0;font-size: 9px; margin-bottom:-20px;}
            #wrapper img {max-width: 200px;width: 80%;}
			.barcode{margin-top: -1px;padding: 0 241px;letter-spacing: 8.5px;}
        }
    </style>
	
	<!-- scripts -->
	<script src="<?=base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>


</head><body>
<!--body-->
<!-- onload="window.print();" -->
    <div id="bill">
		<div id="wrapper">
			<span class="left"><h4><?php if(isset($setting)){echo ucwords($setting['store_name']);} ?></h4></span>
			<span class="right"><!-- <p>Cloth center</p> --><p><!-- Near Govt. Hospital Bhilai --></p></span>
			
			<div class="clearfix"></div>
			
			<h3><center>Medical Bill</center></h3>
			
			<table class="table" cellspacing="0" border="0" style="margin-bottom:5px;">
				<tbody>
					<tr><td colspan="2"><hr></td></tr>
					<tr>
						<td class="left">Store: <?php if(isset($setting)){echo ucwords($setting['store_name']);} ?></td>
						<td class="right">Address: <?php if(isset($setting)){echo $setting['address'];} ?></td>
					</tr>				
					<tr>
						<td class="left">DL No: <?php if(isset($setting)){echo $setting['shop_no'];} ?></td>
						<td class="right">Tel: <?php if(isset($setting)){echo $setting['tel_no'];} ?></td>
					</tr>
					<tr>
						<td colspan="2"><hr></td>                    
					</tr>
					<tr>
						<td class="left" style="font-weight: bold;">Invoice #:<?=$this->input->get('id'); ?></td>
						<td class="right">Date:<?=date("d-m-Y | h:i:s A",strtotime(date('Y-m-d H:i:s'))); ?></td>
						
					</tr>
					<tr>
						<td colspan="2"><hr></td>                    
					</tr>
					<tr>
						<td class="left">Customer:&nbsp;<?php if(isset($pur_order)){ echo ucwords($pur_order['customer']);} ?>
						</td>
					</tr>
					<tr>
						<td class="left">Phone No:&nbsp;<?php if(isset($pur_order)){ echo $pur_order['customer_phone'];} ?>
						</td>
					</tr>
					<tr>
					    <td class="left">Address:&nbsp;&nbsp;&nbsp;<?php if(isset($pur_order)){ echo $pur_order['address'];} ?>
					    </td>
					</tr>
				</tbody>
			</table>
			
			<div class="clearfix"></div>

			<table class="table" cellspacing="0" border="0">
				<thead>
					<tr>
						<th style="text-align:left; padding-left: 5px;"><em>#</em></th>
						<th style="text-align:left">Product</th>                   
						<th>Price</th>
						<th>Quantity</th>
						<th style="text-align:right">Subtotal</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1; if(isset($pur_pro) && count($pur_pro)>0){
                         foreach($pur_pro as $row){
					 ?>		
					<tr>
						<td style="text-align:left; padding-left: 5px;padding-top: 5px; width:30px;"><?=$i;$i++; ?></td>
						<td style="text-align:left; padding-top: 5px;width:180px;"><?=$row['name']; ?>	</td>
						<td style="text-align:center; padding-top: 5px;width:70px;"><?=$row['price']; ?></td>
						<td style="text-align:center; padding-top: 5px;width:50px;"><?=$row['qty']; ?></td>
						<td style="text-align:right; padding-top: 5px;width:70px;"><?=$row['total_amount']; ?></td>
					</tr>
					<?php }} ?>
				</tbody>
			</table>
			
			<div class="clearfix"></div>
			
			<table class="totals" cellspacing="0" border="0" style="margin-bottom:5px;">
				<tbody>
					<tr>
						<td style="text-align:left; font-weight:bold;">Total Items</td>
						<td style="text-align:right; padding-right:1.5%; border-right: 1px solid #000;font-weight:bold;"><?php echo count($pur_pro); ?></td>
						<td style="text-align:left; font-weight:bold;  padding-left:1.5%;">Total</td>
						<td style="text-align:right;font-weight:bold;"><?php if(isset($sum_amount)){ echo $sum= $sum_amount['t_amount'];} ?></td>
					</tr>
					<tr>
						<td style="text-align:left;"></td>
						<td style="text-align:right; padding-right:1.5%; border-right: 1px solid #000;font-weight:bold;"></td>
						<td style="text-align:left; padding-left:1.5%;"></td>
						<td style="text-align:right;font-weight:bold;"></td>
					</tr>
					
					<tr>
						<td colspan="2" style="text-align:left; font-weight:bold; border-top:1px solid #000; padding-top:5px;">Tax</td>
						<td colspan="2" style="border-top:1px solid #000; padding-top:5px; text-align:right; font-weight:bold;"><?php if(isset($pur_order)){ echo $tax=floatval($pur_order['tax']);} ?></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:left; font-weight:bold; border-top:1px solid #000; padding-top:5px;">Grand Total</td>
						<td colspan="2" style="border-top:1px solid #000; padding-top:5px; text-align:right; font-weight:bold;"><?php $gtotal=(($tax*$sum)/100); echo $mgt=$sum+$gtotal; ?></td>
					</tr>
					
					<tr>
						<td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">Discount</td>
						<td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;">- <?php if(isset($pur_order)){ echo $dis=$pur_order['discount'];} ?></td>
					</tr>													
					<tr>
						<td colspan="2" style="text-align:left; font-weight:bold; border-top:1px solid #000; padding-top:5px;">Total Payable</td>
						<td colspan="2" style="border-top:1px solid #000; padding-top:5px; text-align:right; font-weight:bold;"><?=$main=$mgt-$dis; ?></td>
					</tr>
					
										<tr>
						<td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">Total Paid</td>
						<td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;"><?php if(isset($pur_order)){ echo $paid=$pur_order['paid'];} ?></td>
					</tr>
					<tr><td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">Change Due</td>
					<td colspan="2" style="padding-top:5px; text-align:right; font-weight:bold;"><?=$main-$paid;?></td>
					</tr>
									</tbody>
			</table>		
			<div style="border-top:1px solid #000; padding:10px;">			
				Thanks for your business with us!			
			</div>
		</div>	
    </div>
	<br>
	<div id="buttons">
		<center>
		<button class="btn btn-info" onclick="window.print()"; >Print</button>
		    <?php 
		    $users = $this->session->userdata('user_data');
                 if($users['user_type']=='staff'){ ?>
				 <a href="<?=base_url('selling') ?>" class="btn btn-success" >Back to POS Screen</a>
		        <?php }elseif($users['user_type']=='admin'){ ?>
			<a href="<?php echo base_url()."customer"; ?>" class="btn btn-success">Back to All Customer Details</a>	
			<?php } ?>					
		</center>		
	</div>
 <script src="<?=base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?=base_url(); ?>assets/dashboard/dist/js/alertify.js"></script>
    <script type="text/javascript">
       <?php  $data = $this->session->flashdata('responsemsg');
        if($this->session->flashdata('responsemsg') && $data['Status']=='success'){

            echo 'alertify.success("'.$data['msg'].'")';
        }elseif($this->session->flashdata('responsemsg') && $data['Status']=='error'){
            echo 'alertify.error("Sorry:'.$data['msg'].'")';
        }
      ?>   
      </script>

</body></html>