<style type="text/css">
</style>
<div class="content-wrapper">
<style type="text/css">
    .btn-circle1{
        border-radius: 100%;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <?php if($this->input->get('order') || $this->input->get('add')){ ?>
    <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp;<?php echo "Purchase Order"; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <?='Order Purchase';?>
            </li>
        </ol>
        <?php }else{ ?>
        <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp;<?php echo "Purchase List"; ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">
                    <?php echo 'Purchase List' ; ?>
                </li>
            </ol>
            <?php } ?>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-success">
        <?php if($this->input->get('order') || $this->input->get('add')){ ?>
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="pull-right">
                   <a href='<?php echo base_url('products?add='); ?>1' class="btn btn-primary">Add New Product</a>
                    <a href='<?php echo base_url('stock'); ?>' class="btn btn-success">Purchase Lists </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">
                   <input type="hidden" name="auto_click_hide" value="123">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="supl_id">Supplier Name:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <select name="supl_id" class="form-control select2 input-sm">
                                    <option value="">Select Supplier</option>
                                <?php 
                                if(count($vendors)>0){       
                                    foreach($vendors as $row){
                                    ?>
                                    <?php
                                    if($this->input->post('supl_id')&& $this->input->post('supl_id')==$row['id'] ){
                                     $sel="selected";   
                                    }elseif(isset($updateItem) && $row['id']==$updateItem['supl_id']){
                                     $sel="selected";
                                    }else{
                                     $sel="";
                                    } 
                                    ?>
                                    <option value='<?php echo $row['id']; ?>' <?php echo $sel; ?> >
                                            <?php echo $row['name']; ?>
                                    </option>
                                <?php } 
                                } ?>    
                                </select>
                                <?php echo form_error('supl_name','<span class="text-danger">','</span>');?>
                            </div>
                        </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="supl_invoice">Supplier Invoice Number:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                            <input type="text" name="supl_invoice" value='<?php
               echo set_value('supl_invoice',@$updateItem['supl_invoice']);
                ?>' class="form-control input-sm">
                            <?php echo form_error('supl_invoice','<span class="text-danger">','</span>');?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="due_date">Due Date:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <input type="text" name="due_date" id="due_date" value='<?php 
                   if(isset($updateItem)){
                        $date = date("d/m/Y",strtotime($updateItem['due_date']));
                   }
                   echo set_value('due_date',@$date); 
                    ?>' class="form-control input-sm datepicker">
                                <?php echo form_error('due_date','<span class="text-danger">','</span>');?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 table-responsive add_line">
                        <?php if(isset($errormsg)){echo "<span class='text-danger'>".$errormsg."</span>";}?>
                            <table class="table table-striped">
                               <thead>
                                   <tr>
                                        <th style=" width: 2px; ">S.No</th>
                                        <th style="width: 20%;">Product Description:&nbsp;&nbsp;<i class="text-danger">*</i></th>
                                        <th>Supply Quantity:&nbsp;&nbsp;<i class="text-danger">*</i></th>
                                        <th>Unit Supply Cost:&nbsp;&nbsp;<i class="text-danger">*</i></th>
                                        <th>Total Supply Cost:&nbsp;&nbsp;<i class="text-danger">*</i></th>
                                   </tr>
                               </thead>
                               <tbody>
                               <?php $s=1; 
                                if(isset($updateItem['up_pruduct'])){ $up_pruduct = $updateItem['up_pruduct']; }
                               if(isset($updateItem['up_pruduct'])&& count($up_pruduct)>0){
                                foreach($up_pruduct as $value){
                                  $supl_product = $value['product_description'];
                                  $supply_qty = $value['qty'];
                                  $supply_cost = $value['price'];
                                  $total_supply_cost = $value['amount'];
                                  //print_r($supply_cost);die;
                                ?>
                                <tr>
                                  <td><?php echo $s;$s++; ?></td>
                                    <td>
                                        <select name="product_id[]" class="form-control select2 input-sm">
                                            <option value="">Select Product</option>
                                            <?php 
                                                foreach($product as $row){
                                                if(isset($supl_product) && $row['id']==$supl_product){
                                                    $sel1="selected";
                                                }else{
                                                     $sel1="";
                                                } ?>
                                                <option value="<?=$row['id']; ?>" <?=$sel1; ?>> <?=$row['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="supply_qty[]" id="supply_qty" value="<?php if(isset($supply_qty)){ echo $supply_qty; } ?>" class="form-control input-sm supply_calculate">
                                    </td>
                                    <td>
                                        <input type="text" name="supply_cost[]" id="supply_cost" value="<?php if(isset($supply_cost)){ echo $supply_cost; } ?>" class="form-control input-sm supply_calculate">
                                    </td>
                                    <td>
                                        <input type="text" name="total_supply_cost[]" id="total_supply_cost" readonly value="<?php if(isset($total_supply_cost)){
                                                echo $supply_qty*$supply_cost; }
                                                ?>" class="form-control input-sm supply_calculate total_supply_cost">
                                        </td>
                                        <td>
                                        <a href="#" style="z-index: 1000;border-radius: 100%;" class="tip text-center btn btn-warning btn-xs pull-right remove_btn"><i class="fa fa-minus" aria-hidden="true"></i></span></a></td>
                                      </tr>
                               <?php } } ?>
                               </tbody>
                            </table>
                            <a  style="z-index: 1000; border-radius: 100%; cursor: pointer;" class="btn text-center btn-circle btn-success pull-right click_btn"><i class="fa fa-plus"></i></span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="">Total Payable:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <input type="text" readonly name="payable_amt" id="total_payable" value='<?php 
                                echo set_value('payable_amt',@$updateItem['payable_amt']); 
                                ?>' class="form-control input-sm">
                                <?php echo form_error('payable_amt','<span class="text-danger">','</span>');?>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="">Total Paid:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <input type="text" onkeyup="onlynumeric(this)"; name="paid_amt" id="total_paid" value='<?php 
                                    echo set_value('paid_amt',@$updateItem['paid_amt']); 
                                ?>' class="form-control input-sm supply_calculate">
                                <span class="text-danger"><?php echo form_error('paid_amt');?></span>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="">Balance:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <input type="text" name="balance_amt" id="balance_amt" readonly value='<?php 
                                echo set_value('balance_amt',@$updateItem['balance_amt']); 
                                ?>' class="form-control input-sm">
                                <?php echo form_error('balance_amt','<span class="text-danger">','</span>');?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="">Payment Mode:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <select name="pay_mode" id="pay_mode" class="form-control input-sm">
                                    <option value="">Select Payment Mode</option>
                                    <option value="Cash" <?php if($this->input->post('pay_mode')=='Cash'){ echo 'selected';}elseif(isset($updateItem['pay_mode'])>0 && $updateItem['pay_mode']=='Cash'){echo 'selected';} ?> >Cash</option>
                                    <option value="Cheque" <?php if($this->input->post('pay_mode')=='Cheque'){ echo 'selected';}elseif(isset($updateItem['pay_mode'])>0 &&  $updateItem['pay_mode']=='Cheque'){echo 'selected';} ?> >Cheque</option>
                                    <option value="NEFT" <?php if($this->input->post('pay_mode')=='NEFT'){ echo 'selected';}elseif(isset($updateItem['pay_mode'])>0 && $updateItem['pay_mode']=='NEFT'){echo 'selected';}?> >NEFT</option>
                                    <option value="RTGS" <?php if($this->input->post('pay_mode')=='RTGS'){ echo 'selected';}elseif(isset($updateItem['pay_mode'])>0 && $updateItem['pay_mode']=='RTGS'){echo 'selected';} ?> >RTGS</option>
                                </select>
                                <span class="text-danger"><?php echo form_error('pay_mode');?></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12" id="mcheque" <?php if(isset($updateItem['pay_mode'])>0 && $updateItem['pay_mode']=='Cheque'){ echo 'style="display: block;"';  }elseif($this->input->post('pay_mode')=='Cheque'){echo 'style="display: block;"';}else{ echo 'style="display: none;"'; } ?> >
                            <div class="form-group">
                                <label class="control-label" for="cheque_no">Cheque Number:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <input type="text" name="cheque_no" id="cheque_no"value='<?php 
                                if(isset($updateItem['cheque_no'])){
                                    echo $updateItem['cheque_no'];
                                }else{echo set_value('cheque_no'); }
                                ?>' onkeypress="return isNumber(event)" class="form-control input-sm">
                                <span class="text-danger"><?php echo form_error('cheque_no');?></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12" id="mneft" <?php if(isset($updateItem['pay_mode'])>0 && $updateItem['pay_mode']=='NEFT'){}elseif($this->input->post('pay_mode')=='NEFT'){echo 'style="display: block;"';}else{ echo 'style="display: none;"'; } ?>>
                            <div class="form-group">
                                <label class="control-label" for="neft_no">NEFT Unique Trasaction Number (UTR):&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <input type="text" name="neft_no" id="neft_no" value='<?=set_value('neft_no',@$updateItem['neft_tr_id']); ?>' class="form-control input-sm">
                                <span class="text-danger"><?php echo form_error('neft_no');?></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12" id="mrtgs" <?php if(isset($updateItem['pay_mode'])>0 && $updateItem['pay_mode']=='RTGS'){echo 'style="display: block;"';}elseif($this->input->post('pay_mode')=='RTGS'){echo 'style="display: block;"';}else{ echo 'style="display: none;"'; } ?>>
                            <div class="form-group">
                                <label class="control-label" for="rtgs_no">RTGS Unique Trasaction Number (UTR):&nbsp;&nbsp;<i class="text-danger">*</i></label>
                                <input type="text" name="rtgs_no" id="rtgs_no" value='<?=set_value('rtgs_no',@$updateItem['rtgs_tr_id']); ?>' class="form-control input-sm">
                                <span class="text-danger"><?php echo form_error('rtgs_no');?></span>
                            </div>
                        </div>
                    </div>
                    <input type="submit" name="stock" value="Save" class="btn btn-success btn-sm">
                    <a href="stock?u=admin&amp;store=2" class="btn btn-default btn-sm">Cancel</a>
                </form>
            </div>
            <style>
                input[type=radio]:checked+label {
                    background-image: : none;
                    background-color: #d0d0d0;
                }
            </style>
 
  <?php }else{ ?>
        <div class="box-header with-border">
        <span class="col-sm-5 col-xs-5">
            <form method="get">
                <span class="col-sm-5 col-xs-5 col-md-5">
                    <input type="text" name="fromdate" class="datepicker style form-control" name="from_date" placeholder="From date" value="<?php echo $this->input->get("fromdate"); ?>">
                    &nbsp;&nbsp;
                </span>
                <span class="col-sm-5 col-xs-5 col-md-5">
                    <input type="text" name="todate" class="datepicker form-control" name="from_date" placeholder="To date" value="<?php echo $this->input->get("todate"); ?>">
                </span>
                <span class="col-sm-2 col-xs-2 col-md-2">
                    <button type="submit" name="" class="btn btn-sm btn-default">Filter</button>
                </span>
                </form>
        </span>
        <span class="col-sm-4 col-xs-4">
            <form method="get">
                <Select name="membername" onchange="this.form.submit()" class="col-xs-10 select2">
                    <option value="">Select Members</option>
                    <?php
                        if(count($vendors)>0){
                            foreach($vendors as $row){ ?>
                            <option value="<?=$row['id']; ?>" ><?=$row['name']; ?></option>
                        <?php } } ?>
                    <option value="">Select Members</option>
                </select>
            </form>
        </span>
        <span class="col-sm-3 col-xs-3">
            <div class="pull-right">
            <a href='<?=base_url('stock?add='); ?>1'class="btn btn-success">Purchase Order</a>
            </div>
        </span>
        
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <div class="row">
            <div id="printable" class="col-sm-12">
                <table class="table table-striped table-bordered table-condensed table-hover" style="margin-bottom:5px;">
                    <thead>
                        <tr class="active" role="row">
                            <th>S.No</th>
                            <th>Invoice</th>
                            <th>S.Name</th>
                            <th>Product Name</th>
                            <th>Paid</th>
                            <th>Due at</th>
                            <th>Order Date</th>
                            <th>Status</th>
                            <th>Action</th>
                            <th>Payment</th>
                        </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=1; foreach($stock_list as $row){ ?>
                            <tr>
                                <td><?php echo $i;$i++; ?></td>
                                <td><?php echo $row['supl_invoice']; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php
                                 $id = $row['id']; 
                                   $concat = $this->db->query("SELECT GROUP_CONCAT(batch_no SEPARATOR ' + ') AS CO FROM `stock_product` where order_no = '$id' ")->row_array();
                              echo $concat['CO'];
                                 ?></td>
                                <td><?php echo $row['paid_amt']; ?></td>
                                <td><?php echo date("d/m/Y",strtotime($row['due_date']));?></td>
                                <td><?php echo date("d/m/Y",strtotime($row['order_date'])); ?></td>
                                <td><?php if($row['balance_amt']==0){
                                           echo '<h5 class="label label-success">&nbsp;&nbsp;&nbsp;Paid&nbsp;&nbsp;&nbsp;</h5>';  
                                         }elseif($row['balance_amt']<=$row['payable_amt']){
                                         echo'<h5 class="label label-warning">Balance</h5>';
                                      } ?>
                                </td>
                                <td class="no-print">
                                    <center>
                                        <div class="btn-group">
                                           <?php $sessionData = $this->session->userdata('user_data');
                                        ?><a <?php if($sessionData['user_name']=='admin'){echo 'style="display: block;"'; }elseif($sessionData['user_name']!='admin' && $row['is_close']=='close'){echo 'style="display: none;"';}else{echo 'style="display: block;"';} ?>
                                           data-toggle="tooltip" title="Update Here!"  href='<?php echo base_url('stock?order='.$row['id']) ?>' class="tip btn btn-warning btn-xs" data-original-title="Update"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                          </a>
                                        </div>
                                    </center>
                                </td>
                                <td><?php if(!$row['balance_amt']<='0'){ ?>
                                   <button class="tip btn btn-xs btn-info idss" data-id="<?=$row['id'];?>" data-supname="<?=$row['supl_id']?>" data-balence="<?=$row['balance_amt']?>" data-toggle="modal" data-target="#loginModal">Pay</button>
                                   <?php } ?>
                                </td>

                            </tr>
                            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</div>
 <!-- modal here for payments START -->
 <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form id="loginForm" action="<?= base_url('pays_amount'); ?>" method="post" class="form-horizontal">
                    <div class="form-group">
                      <div class="col-xs-12" id="our_modal">
                       <span class="row">
                          <label class="col-xs-2 control-label">Balance:</label>
                          <div class="col-xs-10">
                          <input type="text" id="balance_amt" value="" class="form-control col-xs-10" name="" readonly />
                          </div>
                        </span>
                        <span class="row">
                          <label class="col-xs-2 control-label">Amount:</label>
                          <div class="col-xs-10">
                            <input type="text" onkeyup="onlynumeric(this)"; class="form-control" name="pays_amount" />
                             <input type="hidden" id="find_stock_id" value="" name="pays_id" />
                             <input type="hidden" id="supname" value="" name="supname" />
                        </span>
                    </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-9 text-right">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Pay</button>
                        </div><div class="col-xs-2"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
 <!--modal here for payments END --->
<?php } ?>

</div>
</section>
</div>

