<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-th" aria-hidden="true"></i> <?php echo $title; ?><small></small>
      </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"> <?php echo $title; ?> <?php echo 'Product Report'; ?></li>
            </ol>
    </section>
        <!-- Main content -->
<section class="content">
<!-- filteration add here end -->
        <div class="box">
            <div class="box-header with-border">
                <div class="col-sm-7">
                    <form method="get" action="<?=base_url('perchase-report'); ?>">
                    <span class="col-sm-5 col-xs-5 col-md-5">
                        <input type="text" placeholder="From date" name="fromdate" class="datepicker style form-control" name="from_date" value="<?php echo $this->input->get("fromdate"); ?>">
                        &nbsp;&nbsp;
                    </span>
                    <span class="col-sm-5 col-xs-5 col-md-5">
                        <input type="text" placeholder="To date" name="todate" class="datepicker form-control" name="from_date" value="<?php echo $this->input->get("todate"); ?>">
                    </span>
                    <span class="col-sm-2 col-xs-2 col-md-2">
                        <button type="submit" name="" class="btn btn-sm btn-default">Filter</button>
                    </span>
                </form>
                </div>
                 <div class="pull-right">
                    <button class="btn btn-primary" type="button" onclick="printable('prinatable');">Print</button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body" id="prinatable">
                <table class="table table-striped table-bordered table-condensed table-hover" style="margin-bottom:5px;">
                    <thead>
                        <tr class="active" role="row">
                            <th>S.No.</th>
                            <th>Supplier Invoice No.</th>
                            <th>Supplier Name</th>
                            <th>Paid</th>
                            <th>Due at</th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php $i=1; foreach($stock_list as $row){ ?>
                            <tr>
                                <td><?php echo $i;$i++; ?></td>
                                <td><?php echo $row['supl_invoice']; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['paid_amt']; ?></td>
                                <td><?php echo date("d/m/Y",strtotime( $row['due_date'])); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot><td colspan="6" class="text-right"><?php echo $link; ?></td></tfoot>
                </table>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->