
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-th" aria-hidden="true"></i> <?php echo $title; ?>
        <small><?php echo $section; ?></small>
    </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active"><?php echo $title." "; ?> <?php echo $section; ?></li>
      </ol>
  </section>
        <!-- Main content -->
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <div class="box-header">
          <span class="col-sm-6 col-xs-6">
            <form method="get" action="<?=base_url('sale_report');?>">
              <span class="col-sm-5 col-xs-5 col-md-5">
                <input type="text" name="fromdate" class="datepicker style form-control" name="from_date" placeholder="From date" value="<?php echo $this->input->get("fromdate"); ?>">
                    &nbsp;&nbsp;
              </span>
              <span class="col-sm-5 col-xs-5 col-md-5">
                <input type="text" name="todate" class="datepicker form-control" name="from_date" placeholder="To date" value="<?php echo $this->input->get("todate"); ?>">
              </span>
              <span class="col-sm-2 col-xs-2 col-md-2">
                    <button type="submit" name="" class="btn btn-sm btn-default">Filter</button>
              </span>
            </form>
          </span>
          <span class="col-sm-4">
            <form method="get">
                <select name="paid_by" onchange="this.form.submit()"; class="form-control">
                    <option value="">Select Your Area </option>
                    <option value="cash">Cash</option>
                    <option value="Cheque">Cheque</option>
                </select>
            </form>
        </span>
               </div>
				        <div class="box-body"> 
                  <table id="grid" class="table table-bordered">
                    <thead>
                      <tr>
                            <th>S.No.</th>
                            <th>Invoice No</th>
                            <th>Customer Name</th>
                            <th>Contact No</th>
                            <th>Address</th>
                            <th>Tax</th>
                            <th>Discount</th>
                            <th>Paid-By</th>
                            <th>Total</th>
                            <th>Paid</th>
                            <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if(count($limit)>0){ $i=$limit+1;}else{$i=1;}
                        if(count($customer) >0){ 
                            foreach($customer as $row){
                              
                        ?>
                        <tr>
                            <td><?php echo  $i; ?></td>
                            <td><?php echo  $row['invoice_id']; ?></td>
                            <td><?php echo  $row['customer']; ?></td>
                            <td class="text-center"><?php if(!$row['address']==""){ echo  $row['customer_phone']; }else{ echo '---'; } ?></td>
                            <td class="text-center"><?php if(!$row['address']==""){ echo  $row['address']; }else{ echo '---'; } ?></td>
                            <td><?php echo  $row['tax'];?></td>
                            <td><?php echo  $row['discount'];?></td>
                            <td><?php echo  $row['paidby']; ?></td>
                            <td><?php echo  $row['payable_amt']; ?></td>
                            <td><?php echo  $row['paid']; ?></td>
                            <td><a class="tip btn btn-primary btn-xs" href="<?=base_url('billing?id='.$row['invoice_id']); ?>"><i class="fa fa-files-o" aria-hidden="true"></i></a></td>
                            
                        </tr>
                             <?php
                        $i++; }

                    }else{
                        echo "<tr><td colspan='5' align='center'>Supplier List Not Available.</td></tr>";

                    }?>
                    </tbody>
                    <tfoot><td colspan="14" class="text-right"><?php echo $link; ?></td></tfoot>
                </table>
                
                </div>
              </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->