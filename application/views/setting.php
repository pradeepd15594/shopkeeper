<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content-header" style="color: #00a65a;">
    <h1><i class="fa fa-th" aria-hidden="true"></i> Admin Site Setting:</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url("admin_area"); ?>" style="color: #00a65a;"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active" style="color: #00a65a;">Site Setting</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-success">
        <div class="box-header ui-sortable-handle">
            <a href="<?php echo base_url('add_user'); ?>" style="margin: 1px;" class="btn btn-success pull-right">User List</a>
        </div>
        <div class="content">
            <form id="course-form" method="post"  enctype="multipart/form-data">
              <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <div class="form-group field-batches-start_date required">
                        <label class="control-label" for="batches-start_date">Store Name</label>
                         <input type="text"  class="form-control" name="store_name"
                             value='<?php if(isset($editItem)){ echo $editItem['store_name']; }else{echo set_value('store_name'); } ?>'>
                    </div> 
                </div>
                <div class="col-sm-4 col-xs-4">
                  <div class="form-group field-batches-start_date required">
                    <label class="control-label" for="batches-start_date">DL No:</label>
                      <input type="text" class="form-control" name="shop_no"
                             value='<?=set_value('shop_no',@$editItem['shop_no']);?>'>
                  </div> 
                </div>
                <div class="col-sm-4 col-xs-4">
                  <div class="form-group field-batches-batch_name ">
                    <label class="control-label" for="batches-start_date">Phone No:</label>
                      <input type="text"  class="form-control" name="tel_no"
                             value='<?=set_value('tel_no',@$editItem['tel_no']);?>'>
                             <input type="hidden"  class="form-control" name="id"
                             value='<?=set_value('id',@$editItem['id']);?>'>
                  </div>
                </div>
                <div class="col-sm-4 col-xs-4">
                  <div class="form-group field-batches-batch_name ">
                      <label class="control-label" for="batches-start_date">Address:</label>
                        <input type="text"  class="form-control" name="address"
                               value='<?=set_value('address',$editItem['address']); ?>'>
                  </div>
                </div>
                <div class="col-sm-4 col-xs-4">
                  <div class="form-group field-batches-batch_name ">
                      <label class="control-label" for="batches-start_date">Tax Include:</label>
                        <input type="text"  class="form-control" name="tax_include"
                               value='<?=set_value('tax_include',$editItem['tax_include']);?>'>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <label class="control-label"><hr></label>
                      <button type="submit" class="btn btn-primary btn-create">SUBMIT</button> &nbsp;&nbsp;&nbsp;&nbsp;<button type="reset" name="reset" class="btn btn-danger btn-create">CANCEL</button>
                </div>
              </div>
            </form>
          <hr>
        </div>
      </div> <!-- category listing End -->
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

 