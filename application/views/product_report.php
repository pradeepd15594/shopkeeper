<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header text-success">
        <h1 class="text-success"> <i class="fa fa-th text-success" aria-hidden="true"></i> <?php echo $title; ?><small></small></h1>
            <ol class="breadcrumb">
                <li><a class="text-success" href="<?php echo base_url(); ?>"><i class="fa fa-dashboard text-success"></i>Home</a></li>
                <li class="active"><?php echo 'Product Report'; ?></li>
            </ol>
    </section>
        <!-- Main content -->
<section class="content">
<!-- filteration add here end -->
        <div class="box box-success">
            <div class="box-header with-border">
              <span class="col-sm-8">
               <form method="get">
                    <span class="col-sm-5 col-xs-5 col-md-5">
                        <input type="text" placeholder="From date"  name="fromdate" class="datepicker style form-control" name="from_date" value="<?php echo $this->input->get("fromdate"); ?>">
                        &nbsp;&nbsp;
                        </span>
                        <span class="col-sm-5 col-xs-5 col-md-5">
                        <input type="text" name="todate" class="datepicker form-control" name="from_date" placeholder="To date" value="<?php echo $this->input->get("todate"); ?>">
                        </span>
                        <span class="col-sm-2 col-xs-2 col-md-2">
                        <button type="submit" name="" class="btn btn-sm btn-default">Filter</button>
                        </span>
                </form>
                </span>
                <span class="col-xs-1 col-sm-1 text-right pull-right">
                    <a href="<?=base_url('p-report');?>" class="btn btn-success" type="button"">Reload</a>
                    </span>
                 <span class="col-xs-1 col-sm-1 text-right  pull-right">
                    <button class="btn btn-primary" type="button" onclick="printable('prinatable');">Print</button>
                </span>
            </div><!-- /.box-header -->
            <div class="box-body" id="prinatable">
                <table  class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">S.N</th>
                            <th>Product Name</th>
                            <th>Product Code</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Total Amount</th>
                            <th>Reorder Point</th>
                            <th>created On</th>
                        </tr>
                        <?php if(count($all_data)>0){ $i=1;
                              foreach($all_data as $data){ ?>
                        <tr>
                            <td><?php echo $i;$i++; ?></td>
                            <td><?=$data['name']; ?></td>
                            <td><?=$data['product_code']; ?></td>
                            <td><?=$data['quantity']; ?></td>
                            <td><?=$data['cost']; ?></td>
                            <td><?=$data['cost']*$data['quantity']; ?></td>
                            <td><?=$data['reorder_point']; ?></td>
                            <td><?php echo date('d-m-y', strtotime($data['created_at'])); ?></td>
                        </tr>
                        <?php }
                        }else{
                        echo "<tr><td colspan='5' align='center'>Data List Not Available.</td></tr>";
                        }?>
                    </tbody>
               </table>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->