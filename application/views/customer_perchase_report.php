
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-th" aria-hidden="true"></i> <?php echo 'Patient Report'; ?>
        <small><?php echo $section; ?></small>
    </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active"><?php echo 'Patient Report'; ?></li>
      </ol>
  </section>
        <!-- Main content -->
  <section class="content">
          <div class="box">
            <div class="box-header with-border">
				        <div class="box-body">
                  <table id="grid" class="table table-bordered">
                    <thead>
                      <tr>
                            <th>S.No.</th>
                            <th>Invoice No</th>
                            <th>Customer Name</th>
                            <th>Contact No</th>
                            <th>Address</th>
                            <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if(count($limit)>0){ $i=$limit+1;}else{$i=1;}
                        if(count($customer) >0){ 
                            foreach($customer as $row){
                        ?>
                        <tr>
                            <td><?=$i; ?></td>
                            <td><?=$row['invoice_id']; ?></td>
                            <td><?=$row['customer']; ?></td>
                            <td class="text-center"><?php if(!$row['address']==""){ echo  $row['customer_phone']; }else{ echo '---'; } ?></td>
                            <td class="text-center"><?php if(!$row['address']==""){ echo  $row['address']; }else{ echo '---'; } ?></td>
                            <td><a class="tip btn btn-primary btn-xs" href="<?=base_url('billing?id='.$row['invoice_id']); ?>"><i class="fa fa-files-o" aria-hidden="true"></i></a></td>
                            
                        </tr>
                             <?php
                        $i++; }
                    }else{
                        echo "<tr><td colspan='5' align='center'>Supplier List Not Available.</td></tr>";
                    }?>
                    </tbody>
                    <tfoot><td colspan="9" class="text-right"><?php echo $link; ?></td></tfoot>
                </table>
                </div>
              </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->