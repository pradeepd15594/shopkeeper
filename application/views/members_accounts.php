<?php
 if(count($debit_amount)>0 || count($credit_amount)>0 ) {
 	$amount = $credit_amount['credit_amount'] - $debit_amount['debit_amount'];
 }else{
 	$amount = $totalCradit = $totaldebit = 0;
 }
///////////////////////////////////////////////
$amount = $totalCradit = $totaldebit = 0;
//print_r($member_account_lists);die;
 if(count($member_account_lists)>0){
    $i=0;$html="";
    foreach($member_account_lists as $row){ 
            $i++;
            switch($row['purpose']){
                case 'credit':
                    $amount += $row['amount'];
                    $crad = '<td>'. $row['amount'].'</td>
                        <td></td>';
                    $totalCradit += $row['amount'];
                break;
                case 'debit':
                    $amount -= $row['amount'];
                    $crad = '<td></td><td>'. $row['amount'].'</td>';
                    $totaldebit += $row['amount'];
                break;
            }
$html .= '<tr>
            <td>'.date('Y-m-d H:i:s', strtotime($row['created_at'])) .'</td>
            <td>'. $row['remark'].'</td>
            '.$crad.'
        </tr>';
            //<td><a href="'. base_url("erp_ctrl/member_row/".$row->getId()).'">Account</a></td>
    $i++; }
    }else{
        $html = "<tr><td colspan='4' align='center'>Account Nill.</td></tr>";
}?>
     
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header text-success">
          <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp;<?php echo $title; ?><small><?php echo $section; ?></small></h1>
          <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $section; ?></li>
          </ol>
        </section>
        <section class="content">
			<div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo $section; ?></h3>
				  <div class="pull-right"><a href="<?php echo base_url(); ?>member_list" class="btn btn-success">Supplier List </a></div>
                </div><!-- /.box-header -->
				<?php echo $this->session->flashdata('responseMessage'); ?>
                <div class="box-body">
					<div class="well">
						<div class="col-sm-12">
							<?php $memberName = $membersDetails['name'];
								  $contactNo = $membersDetails['contact_no'];
								  $address = $membersDetails['address'];
								 ?>
							<div class="col-sm-4"><label>Supplier name</label>: &nbsp;<?php echo $memberName; ?></div>
							<div class="col-sm-3"><label>Contact no</label>: &nbsp;<?php echo $contactNo; ?></div>
							<div class="col-sm-5"><label>Address</label>: &nbsp;<?php echo $address; ?></div>
							<div class="col-sm-12"><label>Balance Amount</label>: &nbsp;<?php echo number_format((float)($amount), 2, '.', '')." ".($amount>0? "Credit":"debit"); ?></div>
							
						</div>
						<div class="clearfix"></div>
					</div>
                    <form role="form" method="post">
                        <div>
                            <h4><em><?php echo ucfirst($memberName); ?></em>&nbsp;<small href="#">Ledger Entry</small></h4>
                        </div>
                        <div class="form-group">
                            <label>Amount</label>
                            <input type="text" maxlength="10" onkeyup="onlynumeric(this)"; class="form-control floatNumber" name="amount" value="<?php echo set_value("amount",@$form_data['amount']); ?>" />
                            <?php echo form_error('amount',"<span class='text-danger'>","</span>"); ?>
                        </div>
                        <div class="form-group">
                            <label>Remark</label>
                            <textarea class="form-control" name="remark"><?php echo set_value("amount",@$form_data['amount']); ?></textarea>
                            <?php echo form_error('remark',"<span class='text-danger'>","</span>"); ?>
                        </div>
                        <div class="form-group">
                            <label for="purpose">Purpose</label>
                            <select class="form-control" name="purpose" id="purpose">
								<option value="">Select Purpose</option>
								<option value="debit" <?php if($this->input->post('purpose')=='debit'){ echo 'selected';} ?> >Debit</option>
								<option value="credit" <?php if($this->input->post('purpose')=='credit'){ echo 'selected';} ?>>credit</option>
							</select>
                            <?php echo form_error('purpose',"<span class='text-danger'>","</span>"); ?>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
  </section><!-- /.content -->
  <section class="content">
    <div class="box box-success">
        <div class="col-sm-12">
            <div class="box-header pull-left"> 
          <?php $account=$this->input->get('account'); ?>
                <form method="get" action="<?=base_url('member-accounts'); ?>">
                <label for="fromDate">From date</label>
                <input type="text" name="fromdate" class="datepicker" name="from_date" value="<?php echo $this->input->get('fromdate'); ?>">
                <input type="hidden" name="account"  value="<?php echo $account; ?>">
                &nbsp;&nbsp;
                <label for="fromDate">To date</label>
                <input type="text" name="todate" class="datepicker" name="from_date" value="<?php echo $this->input->get('todate'); ?>">
                <button type="submit" name="" class="btn btn-sm btn-default">Filter</button>
                <a class="btn btn-default btn-sm" href="<?php echo current_url(); ?>">Reset</a>
                </form>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary btn-sm" type="button" onclick="printable('prinatable');">Print</button>
                </div>
            </div>
            <div class="box-body" id="prinatable">
               <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Particular</th>
                                <th>Credit</th>
                                <th>Debit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $html; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2" rowspan="2" style="vertical-align: bottom;" class="text-right">Closing Balance </th>
                                <th><?php echo number_format((float)$totalCradit, 2, '.', ''); ?></th>
                                <th><?php echo number_format((float)$totaldebit, 2, '.', ''); ?></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th><?php echo number_format((float)($totaldebit- $totalCradit), 2, '.', ''); ?></th>
                            </tr>
                        </tfoot>
                    </table>
            </div>
        </div>
    </div>    
  </section>
</div><!-- /.content-wrapper -->