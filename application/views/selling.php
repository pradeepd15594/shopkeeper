<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Medical Shop</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.default.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/dashboard/plugins/jQueryUI/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/custom.css">
    <style>
        body{background-color:#cdcdcd !important;}
        header{color:#fff;}
        div.products{padding:5px;cursor:pointer;}
        .transparent{background: transparent; }
        .nmpd-wrapper {display: none;}
.nmpd-target {cursor: pointer;}
.nmpd-grid {position:absolute; left:50px; top:50px; z-index:5000; -khtml-user-select: none; border-radius:10px; padding:10px; width: initial;}
.nmpd-overlay {z-index:4999;}
input.nmpd-display {text-align: right;}


        .hover {
            position:relative;
        }

        .tooltip { /* hide and position tooltip */
          top:-10px;
          background-color:black;
          color:white;
          border-radius:5px;
          opacity:0;
          position:absolute;
          -webkit-transition: opacity 0.5s;
          -moz-transition: opacity 0.5s;
          -ms-transition: opacity 0.5s;
          -o-transition: opacity 0.5s;
          transition: opacity 0.5s;
        }

        .hover:hover .tooltip { /* display tooltip on hover */
            opacity:1;
        }

        .btn-prod {
            height: 115px;
            width: 24%;
            background: none;
            background: rgba(255, 255, 255, 0.03);
            border: none;
            cursor: pointer;
            /*padding: 2px;*/
            margin: 5px auto;
        }
        .form-control {
    border-radius: 0;
    box-shadow: none;
    border-color: #d2d6de;
}
.todo-list {
    margin: 0;
    padding: 0;
    list-style: none;
    overflow: auto;
}
.todo-list .handle {
    display: inline-block;
    cursor: move;
    margin: 0 5px;
}

.search_control {
    width: 40%;
    height: 30px;
    font-size: 14px;
    color: #dd4b39;
    background-color: #FFF;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
    </style>
</head>
<body class="skin-blue sidebar-collapse">
       <header class="main-header">
        <a href="#" class="logo" style="background-color: rgb(0, 166, 105);">
          <span class="logo-mini"><b>M</b>S</span>
          <span class="logo-lg"><b><i class="fa fa-medkit" aria-hidden="true"></i>&nbsp;Shop-</b>Keeper</span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation" style="background-color: rgb(0, 166, 105);">
          <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
          <div class="navbar-custom-menu" style="background-color: rgb(0, 166, 105);">
            <ul class="nav navbar-nav" style="background-color: rgb(0, 166, 105);">
            <!-- drop down menu start -->
            <?php $users = $this->session->userdata('user_data');
             if($users['user_type']=='staff'){ ?>
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-chevron-down" aria-hidden="true"></i>
              </a>
                  <ul class="dropdown-menu">
                    <li>
                      <ul class="menu">
                        <li><!-- start message -->
                          <a class="bg-blue" href="<?=base_url('staff_cp');?>">
                            <div class="pull-left">
                              <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Change Password</h4>
                          </a>
                        </li>
                         <li><!-- start message -->
                          <a class="bg-green" href="<?=base_url('selling');?>">
                            <div class="pull-left">
                              <i class="fa fa-home" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Home Page</h4>
                          </a>
                        </li>
                        <li><!-- start message -->
                          <a class="bg-red" href="<?=base_url('cbsr');?>">
                            <div class="pull-left">
                              <i class="fa fa-files-o" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Customer Report</h4>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
               </li>
               <?php } ?>
            <!-- drop down menu ends  -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                    <?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url('staff_cp'); ?>" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>

             <?php if($users['user_type']=='admin'){ ?>
               <li><a href="<?=base_url('dashboard'); ?>" data-toggle="control-sidebar"><i class="fa fa-windows" aria-hidden="true"></i></a></li>
              <?php } ?>  
            </ul>
          </div>
        </nav>
      </header>
       <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="col-xs-5">
            <div class="content">
                <div class="box box-solid">
                    <div class="box-header with-border bg-gray">
                        <h3 class="box-title"><i class="fa fa-list"></i>Product list 
                        
                        </h3>
                        <input name="categorySearch" id="search" placeholder="Search Product" class="col-xs-4 form-input pull-right search_control"  value="">
                    </div>
                    <div id="searchContainer" class="box-body search_input product-list" style="max-height:500px;min-height: 500px;overflow-y:auto;" >
                       <?php if(isset($all_product) && count($all_product)>0) { 
                               foreach ($all_product as $row){
                                if($row['quantity']>=$row['reorder_point']){
                            ?>
                        <button class="col-xs-12 col-sm-3  btn-prod tip text-center hover get_product" id="findContainer" data-name="<?=$row['name']; ?>" data-id="<?=$row['id']; ?>" data-qty="1" id="<?=$row['id']; ?>" data-unit_price="<?=$row['cost']; ?>" data-total_price="<?=$row['total_price']; ?>" data->
                            <img src="<?=base_url(); ?>assets/images/22.jpg" class="img-respinsive" style="max-width: 98px;">
                            <span class=""><?php echo substr($row['name'], 0, 17).'..'; ?></span>
                            <span class="tooltip"><?=$row['name'];?>!</span>
                        </button>
                           <?php }} }else{ ?>
                            <div style="position: absolute; width: 100%; height: 100%;padding-top: 25%;top: 0; font-size:16px;"><p class="text-center">No Products found in this Category !</p></div>
                            <?php } ?>                  
                    </div>      
                </div>
            </div>
        </section>
    <section class="col-xs-7">
        <form method="post" enctype="multipart/form-data">
            <div class="content">

                <div class="box box-solid">
                    <div class="box-header list-header with-border bg-gray  text-right">
                        <div class="col-xs-1">X</div>
                        <div class="col-xs-4 text-left">Product</div>
                        <div class="col-xs-2 text-left">Qty</div>
                        <div class="col-xs-2 text-left">Price</div>
                        <div class="col-xs-3 text-left">Total</div>
                        
                    </div>
                    <div class="box-body box-comments text-right" style="min-height: 320px;max-height: 320px; overflow-y: auto; " id="add_product">
                        <!-- here multiple product will be add -->
                    </div>
                    <div class="box-body other_calculate bg-gray">
                        <div class="row" id="remove-trash" >
                            <div class="col-xs-3 text-left">Total Item:</div>
                            <div class="col-xs-3 text-left text-warning" id="totalitem">0</div>
                            <div class="col-xs-4 text-right"><h4>Sub Total:</h4></div>
                            <div class="col-xs-2 text-right text-warning" id="subtotal">0.00</div>
                        </div>
                        <div class="row" >
                            <label class="col-xs-offset-7 col-xs-3 text-right">Tax Included(&#8240;):</label>
                            <div class="col-xs-2">
                                <input class="form-control text-right transparent" type="number" name="tax" id="tax" value="<?php if(isset($tax) && count($tax)>0){ echo $tax['tax_include'];} ?>">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-xs-offset-7 col-xs-3 text-right">Discount(Rs.):</label>
                            <div class="col-xs-2"><input type="number" id="discount" class="form-control text-right transparent" name="discount" value="0.00"></div>
                        </div>
                        <div class="row">
                            <label class="col-xs-offset-7 col-xs-3 text-right">Grand Total:</label>
                            <label class="col-xs-2 text-right" id="gtotal" >0.00</label>
                             <input type="hidden" name="grandtotal" id="grandtotal" value="">
                        </div>
                        <div class="">
                            <span class="col-xs-6 col-xs-offset-6 text-right">
                                <a href="#" id="confirm" data-id="<?php echo base_url('selling'); ?>" class="tip btn btn-danger confirm">Cancel</a>
                                <button type="button" disabled class="btn btn-primary" id="payment" data-toggle="modal" data-target="#myModal">Check Out</button>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- modal here start  -->
<div class="row">
    <div class="modal fade"  id="myModal" role="dialog">
       <div class="modal-dialog"> <!-- Modal content-->
            <div class="modal-content" style="z-index:1000;">
                <div class="modal-header" style="background: #007FFF;">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" id="payModalLabel">Payment</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped" style="margin-bottom:0;">
                    <tbody>
                        <tr>
                            <td>Customer Name :<i style="color:#ff0500;">*</i></td>
                            <td>                                
                                <input type="text" id="cust_name" name="cust_name" class="form-control pcustomer" style="padding: 2px !important; height: auto !important;" aria-haspopup="true" role="textbox">
                            </td>
                        </tr>
                        <tr>
                            <td>Mobile Number :</td>
                            <td>                                
                                <input type="text" id="cust_phone" name="cust_phone" class="form-control" style="padding: 2px !important; height: auto !important;" aria-haspopup="true" onkeyup="onlynumeric(this)"; maxlength="10" role="textbox">
                            </td>
                        </tr>
                        <tr>
                            <td>Address :</td>
                            <td>                                
                                <textarea id="cust_address" name="address" class="form-control" style="padding: 2px !important; height: auto !important;"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>Total Payable Amount :</td>
                            <td id="pay_amount">
                                <span style="background: #FFFF99; padding: 5px 10px; text-weight: bold; color: #000;">
                                    <span id="twt"></span>
                                    <input type="hidden" id="pay_ble" name="pay_ble" value="">
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>Total Purchased Items :</td>
                            <td id="p-count">
                                <span style="background: #FFFF99; text-weight: bold; padding: 5px 10px; color: #000;">
                                    <span id="fcount"></span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>Paid by :</td>
                            <td>
                                <select name="paid_by" id="paid_by" class="form-control" style="padding: 2px !important; height: auto !important;">
                                    <option value="cash">Cash </option>
                                    <option value="CC">Credit Card </option>
                                    <option value="Cheque">Cheque </option>
                                </select>
                            </td>
                        </tr>                       
                        <tr class="pcash">
                            <td>Paid :<i style="color:#ff0500;">*</i></td>
                            <td id="total_paid_amounts">
                                <input type="text" name="paid" id="paid-amount" onkeyup="onlynumeric(this)"; class="form-control ui-keyboard-input ui-widget-content ui-corner-all" style="padding: 2px !important; height: auto !important;" aria-haspopup="true" role="textbox">
                            </td>
                        </tr>
                        <tr class="pcash">
                            <td>Return Change :</td>
                            <td id="retun_change"><span style="background: #FFFF99; padding: 5px 10px; text-weight: bold; color: #000;" id="balance">0.00</span></td>
                        </tr>
                        <tr class="pcc" style="display:none;">
                            <td>Credit Card No :</td>
                            <td>
                                <input type="text" name="card_no" id="pcc" class="form-control" onKeyPress="return isNumber(event)" style="padding: 2px !important; height: auto !important;">
                            </td>
                        </tr>
                        <tr class="pcc" style="display:none;">
                            <td>Credit Card Holder :</td>
                            <td>
                                <input type="text" name="credit_holder" id="pcc_holder" class="form-control" style="padding: 2px !important; height: auto !important;">
                            </td>
                        </tr>
                        <tr class="pcheque" style="display:none;">
                            <td>Cheque No :</td>
                            <td>
                                <input type="text" id="cheque_no" name="cheque" class="form-control" onKeyPress="return isNumber(event)" style="padding: 2px !important; height: auto !important;">
                            </td>
                        </tr>
                        <tr>
                            <td>Invoice type :</td>
                            <td>
                                <select name="invoice_type" id="invoice_type" class="form-control" style="padding: 2px !important; height: auto !important;">
                                    <option value="retail">Retail Invoice</option>
                                    <option value="wholesale">Wholesale Invoice</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button class="btn btn-success" type="submit" disabled id="submit-sale">Done</button>
            </div>
            </div>
        </div>
    </div>
</div>
    <!-- modal here end  -->
            </form>
        </div>
    </section>

      </div>

    <script src="<?=base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?=base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js"></script>
     <script src="<?=base_url(); ?>assets/dashboard/plugins/jQueryUI/jquery-ui.js"></script>
    <script src="<?=base_url(); ?>assets/dashboard/dist/js/alertify.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.numpad.js"></script>
<script type="text/javascript">
    <?php  $data = $this->session->flashdata('responsemsg');
        if($this->session->flashdata('responsemsg') && $data['Status']=='success'){

            echo 'alertify.success("'.$data['msg'].'")';
        }elseif($this->session->flashdata('responsemsg') && $data['Status']=='error'){
            echo 'alertify.error("Sorry:'.$data['msg'].'")';
        }
      ?> 
    $(".confirm").click("click", function () {
          var urls = $(this).attr('data-id');
         alertify.confirm("Are You Sure That You Want To Cancel It?", function (asc) {
            if (asc) {
                window.location.href = urls;
            }
         }, "Default Value");
         return false;
     });
$(document).ready(function(){
    ////  search product here ////////
    $("#search").on('keyup', function(){ 
     var searchVal = $(this).val(); //console.log(searchVal);
     if(searchVal!=""){
        $("#searchContainer").find('button').each(function(){ var searchResult= $(this).attr("data-name").toLowerCase().indexOf(searchVal.toLowerCase()); if(searchResult < 0){ $(this).fadeOut(); }else{ $(this).fadeIn(); } }); }else{ $("#searchContainer").find('button').fadeIn(); }
      });

    //////////   on click button  //////////////all_product_id
    $(document).on('click','.product-list button',function(){
        var name = $(this).attr('data-name');
        var unit_price = $(this).attr('data-unit_price');
        var tprice = $(this).attr('data-total_price');
        var id = $(this).attr('data-id');
        var proid=id;
        var pqty = $(this).attr('data-qty');
        var saleHTML ='<div class="box-comment staff_calculate" id="row-{proid}">'+
                            '<div class="col-xs-1">'+
                                '<a href="javascript:void(0);" class="btn btn-xs btn-default removes" data-id="{proid}"><i class="fa fa-times"></i></a>'
                                +'<input type="hidden" name="all_pro_id[]" value="{proid}" id="all_product_id">'+
                            '</div>'+
                            '<div class="col-xs-4 text-left" id="name">{name}</div>'+
                            '<div class="col-xs-2"><input class="form-control text-center transparent" id="pqty" type="number" name="pqty[]" maxlength="4" style="padding:0px" value="{pqty}"></div>'+
                            '<div class="col-xs-2"><input type="number" id="price" class="form-control text-right transparent" data-price="{unit_price}" name="price[]" value="{unit_price}"></div>'+
                            '<div class="col-xs-3 text-right "  id="tprice" >{tprice}</div>'+
                            '</div>';
        
        if($('#add_product').find("#row-"+id).length==0){
            var t =saleHTML.replace(/{proid}/g,id).replace(/{name}/g,name).replace(/{unit_price}/g,unit_price).replace(/{pqty}/g,pqty).replace(/{tprice}/g,tprice).replace(/{id}/g,id);
            $('#add_product').append(t);
            $('#add_product').find("div#row-"+id).find("#price").numpad({
                decimalSeparator: '.'
            });
           // console.log($('#add_product').find("div#row-"+id));
            $('#add_product').find("div#row-"+id).find("#pqty").numpad({
                decimalSeparator: '.'
            });
            calculate();
            bin();

        }else{
            alertify.error("Sorry:Product already added!"); 
        }
    });

    $('.other_calculate').find("#tax").on('change',function(){
        calculate();
    });
    $('.other_calculate').find("#discount").on('change',function(){
        calculate();
    });



// Set NumPad defaults for jQuery mobile. 
        // These defaults will be applied to all NumPads within this document!
        $.fn.numpad.defaults.gridTpl = '<table class="table modal-content"></table>';
        $.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
        $.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control  input-lg" />';
        $.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn btn-default btn-lg"></button>';
        $.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn btn-lg" style="width: 100%;"></button>';
        $.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};


    
    $('.other_calculate').find("#tax").numpad({
                decimalSeparator: '.'
            });
    $('.other_calculate').find("#discount").numpad({
                decimalSeparator: '.'
            });
    $('.total_paid_amounts').find("#paid-amount").numpad({
                decimalSeparator: '.'
            });

     $("#payment").click(function() {
          var pay_amount= $('.other_calculate').find("#gtotal").html();
          $("#pay_amount").find('#twt').html(pay_amount);
          $("#pay_amount").find('#pay_ble').val(pay_amount);
           var fcount= $('.other_calculate').find("#totalitem").html();
        $("#p-count").find('#fcount').html(fcount);
    });
    $("#paid-amount").on('change',function(){
          calculate();
    });

});//ddocument.ready function close here

function bin(){
    $('.staff_calculate').find("#pqty").on('change',function(){
        calculate();
    });
    $('.staff_calculate').find("#price").on('change',function(){
        calculate();
    });
    $(".removes").click(function(){
            $(this).parents(".staff_calculate").remove();
            calculate();
    });
}

////////////////////////////////////////////////////////////////
///////  count her no of product and its total price  //////////
////////////////////////////////////////////////////////////////
   function calculate(){
      var sum = 0.00;
      var nx = 0;
      $('.staff_calculate').each(function(){
        var parentTr = $(this);
       // var pname = parseFloat(parentTr.find("#pname").val());
        var qty   = parseFloat(parentTr.find("#pqty").val());
        if(parentTr.find("#pqty").val()=="" || qty==0){parentTr.find("#pqty").val(1);qty=1;}
        var price = parseFloat(parentTr.find("#price").val());
        var oldprice = parentTr.find("#price").attr('data-price');
        
        if(parentTr.find("#price").val()=="" || price==0){
            parentTr.find("#price").val(oldprice);price=oldprice;
        }
            var total = qty*price;
            parentTr.find("#tprice").html(total.toFixed(2));
            sum = sum + total; // or: sum += parseFloat(this.value); other_calculate
      });
      
      $("#totalitem").html($('.staff_calculate').length);
      $("#subtotal").html(sum.toFixed(2));
      var tax = parseFloat($("#tax").val());
      //tax validation
        if($("#tax").val()==""){
           parseFloat($("#tax").val(0));tax =0;
        }
      var gtotal = sum+((sum*tax)/100);
      var discount = parseFloat($("#discount").val());
      //discount validation
       if($("#discount").val()==""){
           parseFloat($("#discount").val(0));tax =0;
        }
       gtotal = gtotal - discount;
      $("#gtotal").html(gtotal.toFixed(2));
      $("#grandtotal").val(gtotal);

      var pa= parseFloat($("#paid-amount").val());
      var pays= gtotal-pa;
      $(".pcash").find("#retun_change").find('#balance').html(pays.toFixed(2));
      if($('.staff_calculate').length>0){
        $('#payment').attr('disabled', false);
      }else{ $('#payment').attr('disabled', true);}
      
    }
    </script>
<script type="text/javascript">
function onlynumeric(inputtxt) {
        inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^ 0-9.]+/g, '');
    //inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
 }
    $(document).ready(function(){
        $("#paid_by").change(function() {
            var p_val = $(this).val();
            $('#rpaidby').val(p_val);
            if (p_val == 'cash') {
                $('.pcheque').hide();
                $('.pcc').hide();
                $('.pcash').show();
                $('input[id^="paid-amount"]').keydown(function(e) {                 
                    paid = parseFloat($(this).val());
                    if (e.keyCode == 13) {
                        if (paid < total) {
                            alertify.error("Sorry:Paid amount is less than payable amount");
                            return false
                        }
                        
                        $("#balance").empty();                      
                        var balance = paid - twt;
                        balance = parseFloat(balance).toFixed(2);
                        $("#balance").append(balance);
                        e.preventDefault();
                        return false
                    }
                })
            } else if (p_val == 'CC') {
                $('.pcheque').hide();
                $('.pcash').hide();
                $('.pcc').show()
            } else if (p_val == 'Cheque') {
                $('.pcc').hide();
                $('.pcash').hide();
                $('.pcheque').show()
            } else {
                $('.pcheque').hide();
                $('.pcc').hide();
                $('.pcash').hide()
            }
        });
        $('#submit-sale').click(function(){
          var nrxco =$('.staff_calculate').find("#nrx").html();
          console.log(nrxco);
        if(nrxco=='True'){
           if($('#cust_name').val()=='' || $('#doctor_name').val()=='' || $('#doctor_no').val()=='' || $('#paid-amount').val()==''){
                $('#cust_name').attr('style','border: red solid 1px;');
                $('#doctor_name').attr('style','border: red solid 1px;');
                $('#doctor_no').attr('style','border: red solid 1px;');
                $('#paid-amount').attr('style','border: red solid 1px;');

                alertify.error("All: * field can not be blank !");              
                return false;
            }$('#submit').trigger('click');
        }else{
            if($('#cust_name').val()==''){
                $('#cust_name').attr('style','border: red solid 1px;');
                alertify.error("Customer:field can not be blank!");              
                return false;
            }$('#submit').trigger('click');
        }
        });
    ////////////////////////
     $("#paid-amount").blur(function () {
                checkdata();
        });
        function checkdata() {
           var y = $("#paid-amount").val();
            var x =  parseFloat($("#pay_ble").val());

            console.log(x);
             if(x>y){
                  $('#paid-amount').attr('style','border:red solid 1px;');
                  alertify.error("Paid balance can not be less than to Payable Amount!");
                  $('#submit-sale').attr('disabled', true);
             }else{ $('#submit-sale').attr('disabled', false);}
    }

    });
   </script>
   <script type="text/javascript">
$(document).ready(function(){
    $(".monthPicker").datepicker({ 
        dateFormat: 'mm-yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {  
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val(); 
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val(); 
            $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });    
    });
   });
</style>
</body>
</html>