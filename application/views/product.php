<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<section class="content-header">
  <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp;Product Details
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Category</a></li>
    <li class="active">Channel Category</li>
  </ol>
</section>
  <!-- Main content -->
<section class="content">
  <div class="box box-success">
     <?php if($this->input->get('add')){ ?>
      <div class="box-header ui-sortable-handle">
       <a href="<?=base_url('products'); ?>" class="btn btn-success pull-right" style="margin: 1px;">View Product</a>
        <a href="<?=base_url('category?add='); ?>1" class="btn btn-success pull-right" style="margin: 1px;">Add New Category</a>
     </div>
   <div class="content">
     <form id="course-form" method="post"  enctype="multipart/form-data">
          <div class="row">
            <div class="col-sm-4 col-xs-4">
                <div class="form-group field-batches-start_date required">
                    <label class="control-label" for="batches-start_date">Product Name:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                     <input type="text" class="form-control" name="name" value='<?php if(isset($editItem)){ echo $editItem['name']; }else{ echo set_value('name'); } ?>'>
                    <span class="text-danger"><?php echo form_error('name');  ?></span>
                </div> 
            </div>
             <div class="col-sm-4">
                  <div class="form-group field-batches-batch_name required">
                   <label class="control-label" for="batches-start_date">Product Code:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                    <input type="text" class="form-control" name="product_code" value='<?php if(isset($editItem)){ echo $editItem['product_code']; }else{ echo set_value('product_code'); } ?>'>
                    <span class="text-danger"><?php echo form_error('product_code');?></span>
                  </div>
            </div>
            <div class="col-sm-4 col-xs-4">
                <div class="form-group field-batches-start_date required">
                    <label class="control-label" for="batches-start_date">Product Cost:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                    <input type="text" onkeyup="onlynumeric(this)"; class="form-control" name="cost" value='<?php if(isset($editItem)){ echo $editItem['cost']; }else{ echo set_value('cost'); } ?>'>
                    <span class="text-danger"><?php echo form_error('cost');  ?></span>
                </div>
            </div>
             <div class="col-sm-4 col-xs-4">
                <div class="form-group field-batches-start_date required">
                    <label class="control-label">Product Quantity:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                    <input type="text" onkeyup="onlynumeric(this)"; class="form-control" name="product_quantity" value='<?php if(isset($editItem)){ echo $editItem['quantity']; }else{ echo set_value('product_quantity'); } ?>'>
                    <span class="text-danger"><?php echo form_error('product_quantity'); ?></span>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4">
                <div class="form-group field-batches-start_date required">
                    <label class="control-label">Product Category:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                    <select name="category_type" class="form-control">
                      <option value=""> Select Your Area </option>
                      <?php if(count($product_name)>0){ 
                        foreach ($product_name as $row){
                        if($this->input->post('category_type')==$row['medicine_type_name']){
                          $sel="Selected";
                        }elseif(isset($editItem['category_type']) && $editItem['category_type']==$row['medicine_type_name']){
                          $sel="Selected";
                        }else{ $sel="";}
                       ?>
                      <option value="<?=$row['id']; ?>" <?php echo $sel;?> ><?=$row['category']?></option>
                      <?php }} ?>                
                      </select>
                    <span class="text-danger"><?php echo form_error('category_type'); ?></span>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4">
                <div class="form-group field-batches-start_date required">
                  <label class="control-label">Reorder Point:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                  <input type="text" onkeyup="onlynumeric(this)"; class="form-control" name="reorder_point" value='<?php echo set_value('reorder_point',@$editItem['reorder_point']);?>'>
                  <span class="text-danger"><?php echo form_error('reorder_point'); ?></span>
                </div>
            </div>
        </div>
        <!-- new row here -->
        <div class="row">
          <div class="col-sm-4 col-xs-4">
            <label class="control-label"><hr></label>
              <button type="submit" class="btn btn-success btn-create">SUBMIT</button>
              <button type="reset" name="reset" class="btn btn-danger btn-create">CANCEL</button>
          </div>
        </div>
  </form>
  <!-- category listing start -->
  <?php }else{ ?>
  <div class="box-header ui-sortable-handle">
   <h4></h4><a href="<?php echo base_url('products?add='); ?>1" class="btn btn-primary pull-right">Add Product</a>
 </div>
  <div class="box-body">
  <div class="row">
            <div id="printable" class="col-md-12">
                <table class="table table-striped table-condensed table-hover" style="margin-bottom:5px;">
                    <thead>
                        <tr class="active" role="row">
                          <th>Product Name</th>
                          <th>Product Code</th>
                          <th>Product Cost</th>
                          <th>Product Quantity</th>
                          <th>Total Price</th>
                          <th>Action</th>
                        </tr>
                    </thead>
                    <tbody aria-live="polite" aria-relevant="all">
                    <?php $i=1;  if(isset($list_item)){
                        foreach($list_item as $row){
                     ?>
                        <tr>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['product_code']; ?></td>
                            <td><?php echo $row['cost']; ?></td>
                            <td><?php echo $row['quantity']; ?></td>
                            <td><?php echo $row['total_price']; ?></td>
                            <td>
                               <a data-toggle="tooltip" title="Product Update!" href="<?=base_url('products?add='.$row['id'].'&product_up='.$row['id']); ?>" class="tip btn btn-success btn-xs"><i class="fa fa-pencil-square" aria-hidden="true"></i>
                               </a>
                               <a data-toggle="tooltip" title="Delete!" href="#" id="confirm" data-id="<?php echo base_url('products?pro_trash='.$row['id']); ?>" class="tip btn btn-danger btn-xs confirm"><i class="fa fa-trash-o"></i>
                               </a>
                               
                            </td>
                        </tr>
              <?php } } ?>
                    </tbody>
                </table>
            </div>
        </div>
     </div>
     <?php } ?>
     </div>
     </div>
  <!-- category listing End -->
 </div>
  <!-- Your Page Content Here -->

</section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 