 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Change Password</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.default.css">
 </head>
 <body class="skin-blue sidebar-collapse">
     <header class="main-header">
        <a href="#" class="logo" style="background-color: rgb(0, 166, 105);">
          <span class="logo-mini"><b>S</b>K</span>
          <span class="logo-lg"><b><i class="fa fa-medkit" aria-hidden="true"></i>&nbsp;Shop-</b>Keeper</span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation" style="background-color: rgb(0, 166, 105);">
          <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav" > 
            <!-- drop down menu start -->
            <?php $users = $this->session->userdata('user_data');
             if($users['user_type']=='staff'){ ?>
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-chevron-down" aria-hidden="true"></i>
              </a>
                  <ul class="dropdown-menu">
                    <li>
                      <ul class="menu">
                        <li><!-- start message -->
                          <a class="bg-blue" href="<?=base_url('staff_cp');?>">
                            <div class="pull-left">
                              <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Change Password</h4>
                          </a>
                        </li>
                         <li><!-- start message -->
                          <a class="bg-green" href="<?=base_url('selling');?>">
                            <div class="pull-left">
                              <i class="fa fa-home" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Home Page</h4>
                          </a>
                        </li>
                        <li><!-- start message -->
                          <a class="bg-red" href="<?=base_url('cbsr');?>">
                            <div class="pull-left">
                              <i class="fa fa-files-o" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Customer Report</h4>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
               </li>
               <?php } ?>
            <!-- drop down menu ends  -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header" style="background-color: rgb(0, 166, 105);">
                    <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                    <?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url('staff_cp'); ?>" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <?php if($users['user_type']=='admin'){ ?>
               <li><a href="<?=base_url('dashboard'); ?>" data-toggle="control-sidebar"><i class="fa fa-windows" aria-hidden="true"></i></a></li>
              <?php } ?>  
            </ul>
          </div>
        </nav>
      </header>
 <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 572px;">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1></h1>
            </section>
            <!-- Main content -->
            <section class="content" style="margin: 0px 300px;">
              <div class="box box-success">
                    <form  method="post">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                   <h3 class="login-box-msg" style="padding-bottom: -2px;">Change Password</h3> 
                                    <p><span class="text-danger"> <?php echo $this->session->flashdata('message'); ?></span></p>
                                <form  method="post">

                                  <div class="form-group has-feedback">
                                    <input type="password" name="old_password" class="form-control pradeep" value="<?=set_value('old_password'); ?>" placeholder="Old Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?php echo form_error('old_password', '<div class="text-danger">', '</div>'); ?>
                                  </div>
                                     <div class="form-group has-feedback">
                                    <input type="password" name="newpassword" class="form-control pradeep" placeholder="New Password" value="<?=set_value('newpassword'); ?>">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?php echo form_error('newpassword', '<div class="text-danger">', '</div>'); ?>
                                  </div>
                                     <div class="form-group has-feedback">
                                    <input type="password" name="re_password" class="form-control pradeep" placeholder="Confirm New Password" value="<?=set_value('re_password'); ?>">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?php echo form_error('re_password', '<div class="text-danger">', '</div>'); ?>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <button type="submit" class="btn btn-primary">CHANGE</button>
                                      <a href="<?=base_url('selling');?>" class="btn btn-success">Back to POS Screen</a>  
                                    </div>
                                      <div class="col-xs-8">
                                      <span class="text-danger"><?php if(isset($error_msg)){ echo $error_msg; }?></span> 
                                      </div><!-- /.col -->
                                  </div>
                                </form>
                                </div>
                            </div> 
                        </div>
                    </form>
                  </div>  
            </section>
        </div>
        <!-- /.content-wrapper -->
<script src="<?=base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
 <script src="<?=base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?=base_url(); ?>assets/dashboard/dist/js/alertify.js"></script>
    <script type="text/javascript">
       <?php  $data = $this->session->flashdata('responsemsg');
        if($this->session->flashdata('responsemsg') && $data['Status']=='success'){

            echo 'alertify.success("'.$data['msg'].'")';
        }elseif($this->session->flashdata('responsemsg') && $data['Status']=='error'){
            echo 'alertify.error("Sorry:'.$data['msg'].'")';
        }
      ?>   
      </script>

</body></html>
        <!-- Control Sidebar -->