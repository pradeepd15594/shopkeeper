      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp;<?php echo $title; ?>
            <small><?php echo $section; ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active"><?php echo $title." "; ?> <?php echo $section; ?></li>
          </ol>
        </section>
        <?php if($this->session->flashdata('responseMessage')){ ?>
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i><?php echo $this->session->flashdata('responseMessage'); ?></h4>
          </div>
          <?php } ?>
        <!-- Main content -->
        <section class="content">
          <div class="box">
                <div class="box-header with-border">
                   <div class="pull-left">
                <form method="get" action="<?=base_url('member_list'); ?>">
                  <Select name="membername" class="col-sm-12 input-sm select2" onchange="this.form.submit()">
                      <option value="">Select Supplier</option>
                      <?php
                          if(count($members_all)>0){
                              foreach($members_all as $row){
                                  echo "<option value='".$row['id']."'>".$row['name']."(".$row['contact_no'].")</option>";
                              }
                          }
                      ?>
                      <option value="">Select Supplier</option>
                  </select>
          </form>
            </div>
            <div class="pull-right">
              <a href="<?php echo base_url(); ?>add_member" class="btn btn-success">Add New Supplier </a></div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="grid" class="table table-bordered">
                    <thead>
                      <tr>
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Contact No</th>
                            <th>Address</th>
                            <th>Supplier Account</th>
                            <th style="width: 40px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($members) >0){ $i=1;
                            foreach($members as $member){
                        ?>
                        <tr>
                            <td><?php echo  $i++; ?></td>
                            <td><?php echo  $member['name']; ?></td>
                            <td><?php echo  $member['contact_no']; ?></td>
                            <td><?php echo  $member['address']; ?></td>
                            <td><a href="<?php echo base_url('member-accounts?account='.$member['id']); ?>" >Account&nbsp;&nbsp;<i class="fa fa-calculator" aria-hidden="true"></i></a></td>
                            <td><a href="<?=base_url('add_member?m_up='.$member['id']); ?>"  data-toggle="tooltip" data-placement="left" title="Update..!" class="tip btn btn-success btn-xs"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                             <?php
                        $i++; }

                    }else{
                        echo "<tr><td colspan='5' align='center'>Supplier List Not Available.</td></tr>";

                    }?>
                    </tbody>
                </table>
                </div>
              </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->