<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"><!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="text-success"><i class="fa fa-th" aria-hidden="true"></i> Product Category</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard text-success"></i>Dashboard > Category</a></li>
      </ol>
  </section>
<!-- Main content -->
<section class="content">
  <div class="box box-success">
    <?php if($this->input->get('add')){ ?>
      <div class="box-header ui-sortable-handle">
        <div class="col-sm-12 col-xs-12">
          <span class="pull-right"><a class="tip btn-success btn" href="<?=base_url('category'); ?>" >View List</a>
          </span> 
        </div> 
      </div>
          <div class="content">
            <form id="course-form" method="post">
              <div class="row">
                <div class="col-sm-4 col-xs-4">
                  <div class="form-group field-batches-start_date required">
                      <label class="control-label">Category Name:&nbsp;&nbsp;<i class="text-danger">*</i>
                      </label>
                      <input type="text" class="form-control" name="medicine" value='<?=set_value('medicine',@$editItem['medicine_type_name']); ?>'>
                      <span class="text-info">Eg: Capsoole, Tablet, Sering</span>
                      <span class="text-danger"><?php echo form_error('medicine');  ?></span>
                  </div>
                </div> 
                <div class="col-sm-4 col-xs-4">
                  <label class="control-label"><hr></label>
                  <button type="submit" class="btn btn-success btn-create">SUBMIT</button> &nbsp;&nbsp;&nbsp;&nbsp;
                  <button type="reset" name="reset" class="btn btn-danger">Reset</button>
                </div>
              </div>
            </form>
            <?php }else{ ?>
            <div class="box-body">
              <div class="col-sm-12">
                <span class="pull-right"><a class="tip btn-success btn" href="<?=base_url('category?add='); ?>1" >Add Category</a></span>
              </div><br><br>
                <div class="row">
                  <div id="printable" class="col-md-12">
                    <table class="table table-striped table-bordered table-condensed table-hover" style="margin-bottom:5px;">
                        <thead>
                            <tr class="active" role="row">
                                <th>S.No</th>
                                <th>Category Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php if(count($limit)>0){ $i=$limit+1;}else{$i=1;}
                              if(isset($medicine)){
                              foreach($medicine as $row){
                         ?>
                        <tr>
                            <td><?=$i;$i++; ?></td>
                            <td><?=$row['category']; ?></td>
                            <td>
                               <a href="#" id="confirm" data-id="<?php echo base_url('category?cdel='.$row['id']); ?>" class="tip btn btn-danger btn-xs confirm"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                      <?php } } ?>
                    </tbody>
                    <tfoot>
                      <td colspan="3" class="text-right">
                        <?php echo $link; ?>
                      </td>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

 