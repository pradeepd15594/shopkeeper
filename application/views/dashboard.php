      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Dashboard</h1>
  <ul>
  </ul>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
</section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-facebook rounded">
                <span class="mini-stat-icon"><i class="fa fa-users fg-facebook"></i></span><!-- /.mini-stat-icon -->
                <div class="mini-stat-info">
                    <span><?php $cust = $this->db->query("SELECT * FROM `customers`")->result_array();
                       echo count($cust);
                     ?></span>
                    <abbr><i class="fa fa-bell-slash-o" aria-hidden="true"></i>&nbsp;Total Customers</abbr>
                </div><!-- /.mini-stat-info -->
            </div><!-- /.mini-stat -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-twitter rounded">
                <span class="mini-stat-icon"><i class="fa fa-medkit fg-twitter"></i></span><!-- /.mini-stat-icon -->
                <div class="mini-stat-info">
                    <span><?php $cust = $this->db->query("SELECT * FROM `products`")->result_array();
                       echo count($cust);
                     ?></span>
                    <i class="fa fa-bell-slash-o" aria-hidden="true"></i>&nbsp; Total Medicines
                </div><!-- /.mini-stat-info -->
            </div><!-- /.mini-stat -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-googleplus rounded">
                <span class="mini-stat-icon"><i class="fa fa-users fg-googleplus"></i></span><!-- /.mini-stat-icon -->
                <div class="mini-stat-info">
                    <span><?php $cust = $this->db->query("SELECT * FROM `members`")->result_array();
                       echo count($cust);
                     ?></span>
                   <i class="fa fa-bell-slash-o" aria-hidden="true"></i>&nbsp; Total Supplier
                </div><!-- /.mini-stat-info -->
            </div><!-- /.mini-stat -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="mini-stat clearfix bg-bitbucket rounded">
                <span class="mini-stat-icon"><i class="fa fa-inr fg-bitbucket"></i></span><!-- /.mini-stat-icon -->
                <div class="mini-stat-info">
                    <span><?php if(isset($today_sale) && count($today_sale)>0){
                       echo $today_sale['total_amount'];
                        
                    }
                     ?></span>
                    
                   <i class="fa fa-bell-slash-o" aria-hidden="true"></i>&nbsp; Total Today Sale
                </div>
            </div><!-- /.mini-stat-info -->
        </div><!-- /.mini-stat -->        
                        
                        
          </div> <!-- row end here -->
        <!-- WHEN THE STOCK AVILABLE THEN CHECK AND INFORMATION -->
        <div class="box box-success">
    <div class="box-header with-border">
              <h3 class="box-title">Stock Available</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <ul class="todo-list ui-sortable">
              <?php if(count($stock_msg)>0){ 
                      foreach ($stock_msg as $row){  
                  ?> 
                <li>
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                    <!-- todo text -->
                    <span class="text"><?=ucwords($row['name']);?></span>
                    <!-- Emphasis label -->
                  <small class="label label-danger"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></small>&nbsp;&nbsp;Quantity Are Out of stock only <i class="text-danger"><?=$row['quantity'];?></i> No. avilable click here to update Stock.
                  <div class="tools">
                    <a href="<?=base_url('stock?add='); ?>1" class="tip btn btn-success btn-xs"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                  </div>
                </li>
                <?php }} ?>
              </ul>
            </div>
          </div>
          <!-- GRAPG HERE OF WHOLE MEDICIENS-->
          <div class="box box-primary">
          <div class="box-body">
             <div class="col-md-12 col-xs-12">
              <div class="box-header with-border">
                <h3 class="box-title">Line Chart</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body chart-responsive">
               <div class="col-md-12 col-xs-12">
                 <div id="line-example" style="height: 300px;"></div>
               </div>
              </div>
              </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 