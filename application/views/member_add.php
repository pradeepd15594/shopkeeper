<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp; <?php echo $title; ?><small><?php echo $section; ?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><?php echo $title; ?> <?php echo $section; ?></li>
    </ol>
  </section>
        <!-- Main content -->
  <section class="content">
		<div class="box box-success">
        <div class="box-header with-border">
					<h3 class="box-title"><?php echo $section; ?>
					</h3>
					<div class="pull-right"><a href="<?php echo base_url()."member_list"; ?>" class="btn btn-info">Supplier List </a>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label>Name:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                <input type="text" name="name" class="form-control" placeholder="Enter Full Name" value="<?=set_value('name',@$preData['name']);?>">
                <?php echo form_error('name','<span class="text-danger">','</span>')?>
            </div>
            <div class="form-group">
                <label>Address:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                <textarea name="address" class="form-control"><?=set_value('address',@$preData['address']);?></textarea>
                <?php echo form_error('address','<span class="text-danger">','</span>')?>
            </div>
            <div class="form-group">
                <label>Contact No:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                <input type="text" name="contact" class="form-control" placeholder="Enter Contact No" maxlength="10" onkeyup="onlynumeric(this)"; value="<?=set_value('contact',@$preData['contact_no']);?>">
                <?php echo form_error('contact','<span class="text-danger">','</span>')?>
            </div>
            <?php if($this->input->get('edit') && ($this->input->get('edit'))){
            		echo '<input type="hidden"  name="member_id" value="'.$this->input->get('edit').'">';
            	 }?>
            <div class="box-footer">
            	<button type="submit" class="btn btn-flat btn-sm btn-success">Save</button>&nbsp;
            	<button type="reset" class="btn btn-flat btn-sm btn-danger">Reset</button>
            </div>
          </form>
        </div><!-- /.box-body -->
      </div>
 </section><!-- /.content -->
</div><!-- /.content-wrapper -->