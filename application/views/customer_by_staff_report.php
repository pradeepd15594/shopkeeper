 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Customer Report</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.default.css">
 </head>
 <body class="skin-blue sidebar-collapse">
     <header class="main-header">
        <a href="#" class="logo" style="background-color: rgb(0, 166, 105);">
          <span class="logo-mini"><b>M</b>S</span>
          <span class="logo-lg"><b><i class="fa fa-medkit" aria-hidden="true"></i>&nbsp;Shop-</b>Keeper</span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation" style="background-color: rgb(0, 166, 105);">
          <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav" >
             <!-- drop down menu start -->
            <?php $users = $this->session->userdata('user_data');
             if($users['user_type']=='staff'){ ?>
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-chevron-down" aria-hidden="true"></i>
              </a>
                  <ul class="dropdown-menu">
                    <li>
                      <ul class="menu">
                        <li><!-- start message -->
                          <a class="bg-blue" href="<?=base_url('staff_cp');?>">
                            <div class="pull-left">
                              <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Change Password</h4>
                          </a>
                        </li>
                         <li><!-- start message -->
                          <a class="bg-green" href="<?=base_url('selling');?>">
                            <div class="pull-left">
                              <i class="fa fa-home" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Home Page</h4>
                          </a>
                        </li>
                        <li><!-- start message -->
                          <a class="bg-red" href="<?=base_url('cbsr');?>">
                            <div class="pull-left">
                              <i class="fa fa-files-o" aria-hidden="true"></i>
                            </div><h4 style="color:#FFF;">Customer Report</h4>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
               </li>
               <?php } ?>
            <!-- drop down menu ends  -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header" style="background-color: rgb(0, 166, 105);">
                    <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                    <?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url('staff_cp'); ?>" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <?php if($users['user_type']=='admin'){ ?>
               <li><a href="<?=base_url('dashboard'); ?>" data-toggle="control-sidebar"><i class="fa fa-windows" aria-hidden="true"></i></a></li>
              <?php } ?>  
            </ul>
          </div>
        </nav>
      </header>
 <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 572px;">
            <!-- Main content -->
            <section class="content">
          <div class="box">
            <div class="box-header with-border">
            <form method="get">
                <div class="content-header col-xs-6">
                <span class="inline col-xs-7">
                   <input type="text" name="cust_names" placeholder="Search By customer name" class="form-control text-danger">
                   </span>
                   <label class="inline col-xs-2"><button type="button" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</button></label>
                   <label class="inline col-xs-2"><a href="<?=base_url('cbsr');?>" class="btn btn-default"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Refresh</a></label>
                </div>
                </form>
                <div class="box-body">
                  <table id="grid" class="table table-bordered">
                    <thead>
                      <tr>
                            <th>S.No.</th>
                            <th>Invoice No</th>
                            <th>Customer Name</th>
                            <th>Contact No</th>
                            <th>Address</th>
                            <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if(count($limit)>0){ $i=$limit+1;}else{$i=1;}
                        if(count($customer) >0){ 
                            foreach($customer as $row){
                        ?>
                        <tr>
                            <td><?=$i; ?></td>
                            <td><?=$row['invoice_id']; ?></td>
                            <td><?=$row['customer']; ?></td>
                            <td class="text-center"><?php if(!$row['address']==""){ echo  $row['customer_phone']; }else{ echo '---'; } ?></td>
                            <td class="text-center"><?php if(!$row['address']==""){ echo  $row['address']; }else{ echo '---'; } ?></td>
                            <td><a class="tip btn btn-primary btn-xs" href="<?=base_url('billing?id='.$row['invoice_id']); ?>"><i class="fa fa-files-o" aria-hidden="true"></i></a></td>
                            
                        </tr>
                             <?php
                        $i++; }
                    }else{
                        echo "<tr><td colspan='5' align='center'>Supplier List Not Available.</td></tr>";
                    }?>
                    </tbody>
                    <tfoot><td colspan="9" class="text-right"><?php echo $link; ?></td></tfoot>
                </table>
                </div>
              </div>
        </section>
        </div>
        <!-- /.content-wrapper -->
<script src="<?=base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
 <script src="<?=base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?=base_url(); ?>assets/dashboard/dist/js/alertify.js"></script>
    <script type="text/javascript">
       <?php  $data = $this->session->flashdata('responsemsg');
        if($this->session->flashdata('responsemsg') && $data['Status']=='success'){

            echo 'alertify.success("'.$data['msg'].'")';
        }elseif($this->session->flashdata('responsemsg') && $data['Status']=='error'){
            echo 'alertify.error("Sorry:'.$data['msg'].'")';
        }
      ?>   
      </script>

</body></html>
        <!-- Control Sidebar -->