 <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp;Privacy</h1>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="box box-success">
                    <form  method="post">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                   <h3 class="login-box-msg" style="padding-bottom: -2px;">Change Password</h3> 
                                    <p><span class="text-danger"> <?php echo $this->session->flashdata('message'); ?></span></p>
                                <form  method="post">

                                  <div class="form-group has-feedback">
                                    <input type="password" name="old_password" class="form-control pradeep" placeholder="Old Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?php echo form_error('old_password', '<div class="text-danger">', '</div>'); ?>
                                  </div>
                                     <div class="form-group has-feedback">
                                    <input type="password" name="newpassword" class="form-control pradeep" placeholder="New Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?php echo form_error('newpassword', '<div class="text-danger">', '</div>'); ?>
                                  </div>
                                     <div class="form-group has-feedback">
                                    <input type="password" name="re_password" class="form-control pradeep" placeholder="Confirm New Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?php echo form_error('re_password', '<div class="text-danger">', '</div>'); ?>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-4">
                                      <button type="submit" class="btn btn-primary">CHANGE</button>
                                    </div>
                                      <div class="col-xs-8">
                                      <span class="text-danger"><?php if(isset($error_msg)){ echo $error_msg; }?></span> 
                                      </div><!-- /.col -->
                                  </div>
                                </form>
                                </div>
                            </div> 
                        </div>
                    </form>
                  </div>  
            </section>
            <!---end main content----->
        </div>
        <!-- /.content-wrapper -->
        
<script type="text/javascript">
function onlyalphabates(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/[^a-zA-Z .]+/g, '');
}
function onlynumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^ 0-9]+/g, '');
 // inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}
function alphanumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}
</script>
        <!-- Control Sidebar -->