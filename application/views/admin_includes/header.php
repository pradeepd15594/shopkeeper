<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php if(isset($title)){ echo $title;} ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/alertify.default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/dist/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/dashboard/plugins/jQueryUI/jquery-ui.css">
    <script src="<?=base_url();?>assets/dashboard/plugins/jQueryUI/jquery-1.9.1.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <a href="index2.html" class="logo">
          <span class="logo-mini"><b>S</b>K</span>
          <span class="logo-lg"><b>Shop</b>keeper</span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php $stock_msg= $this->db->query("SELECT * FROM `products` WHERE `quantity`<=`reorder_point` ORDER BY `name` ASC")->result_array();
                //print_r($stock_msg);die;
        ?>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?=count($stock_msg); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?=count($stock_msg); ?> Notifications</li>
               <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <?php if(count($stock_msg)>0){ 
                      foreach ($stock_msg as $row){  
                  ?>
                  <li><a class="bg-danger" href="#"><i class="fa fa-truck text-aqua"></i><?=ucwords($row['name']); ?> are <?=$row['quantity']; ?> which are Less Than To Reorder Point <?=$row['reorder_point']; ?> new members joined  today</a>
                  </li>
                <?php }} ?>
                </ul>
               </li>
              <li class="footer"><a href="#"></a></li>
            </ul>
          </li>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php $users = $this->session->userdata('user_data');
                    echo ucwords($users['name']);?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                    <?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url('key-change'); ?>" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <li>
                <a href="<?=base_url('dashboard'); ?>"><i class="fa fa-home"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>