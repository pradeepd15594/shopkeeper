     <!-- Main Footer -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          Anything you want
        </div>
        <strong>Copyright &copy; 2015 <a href="#">Company</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->
    <script src="<?php echo base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?=base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url(); ?>assets/dashboard/dist/js/app.min.js"></script>
    <script src="<?=base_url(); ?>assets/dashboard/dist/js/alertify.js"></script>
    <script src="<?=base_url(); ?>assets/dashboard/dist/js/dashboard.js"></script>
    <script src="<?=base_url(); ?>assets/dashboard/plugins/select2/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dashboard/plugins/jQueryUI/jquery-ui.js"></script>

    <script src="<?=base_url(); ?>assets/js/custom.js"></script>

    <script type="text/javascript">
    //when we delete any row with table then this message function are run
     $(".confirm").click("click", function () {
      var urls = $(this).attr('data-id');
     alertify.confirm("Are You Sure That You Want To Delete This?", function (asc) {
         if (asc) {
           //alertify.success("Record is deleted.");
         window.location.href = urls; 
             //ajax call for delete       
             //alertify.success("You clicked Ok.");

         }
     }, "Default Value");
     return false;
 });
    // success notification message when eny php query are run
    <?php  $data = $this->session->flashdata('responsemsg');
        if($this->session->flashdata('responsemsg') && $data['Status']=='success'){

            echo 'alertify.success("'.$data['msg'].'")';
        }elseif($this->session->flashdata('responsemsg') && $data['Status']=='error'){
            echo 'alertify.error("Sorry:'.$data['msg'].'")';
        }
      ?>   
      </script>
<script type="text/javascript">
$(document).ready(function() {
  $(".select2").select2();
});
/////////////////////////////////////////////////////////////////////
/////////////    multiple product add here with js      /////////////
/////////////////////////////////////////////////////////////////////
   <?php $i=1;?>
   var templets='<tr>'+
      '<td>{sno}</td>'+
            '<td>'
                +'<select name="product_id[]" class="form-control select2 input-sm">'+
                        '<option value="">Select Product</option>'+
                        '<?php $product = $this->db->query("SELECT * FROM `products` where `name` != ''")->result_array();
                                  foreach($product as $row){ 
                        if(isset($updateItem) && $row['id']==$updateItem['product_id']){
                                                 $sel1="selected";
                                                }elseif($this->input->post('product_id[]') && $this->input->post('product_id[]')==$row['id'] ){
                                                 $sel1="selected";   
                                                }else{
                                                 $sel1="";
                                                } 
                                                
                           echo '<option value="'. $row['id'].'" '.$sel1 .'> '.$row['name'].' </option>';
                             } ?>'+
                    '</select>'+
                    '<span class="text-danger"><?php echo form_error("product_id");?></span>'+
            '</td>'+
            '<td>'+
                '<input type="text" onkeyup="onlynumeric(this)"; name="supply_qty[]" id="supply_qty" value="<?php 
                                    if(isset($updateItem)){
                                        
                                    }else{echo set_value('supply_qty[]'); }
                                    ?>" class="form-control input-sm supply_calculate">'+
                 '<span class="text-danger"><?php echo form_error('supply_qty');?></span>'+
            '</td>'+
            '<td>'+
                '<input type="text" onkeyup="onlynumeric(this)"; name="supply_cost[]" id="supply_cost" value="<?php 
                                    if(isset($updateItem)){
                                        
                                    }else{echo set_value('supply_cost[]'); }
                                    ?>" class="form-control input-sm supply_calculate">'+
                    '<span class="text-danger"><?php echo form_error('supply_cost');?></span>'+
            '</td>'+
            '<td>'+
                '<input type="text" name="total_supply_cost[]" id="total_supply_cost" readonly value="<?php 
                                    if(isset($updateItem)){
                                        
                                    }else{echo set_value('total_supply_cost[]'); }
                                    ?>" class="form-control input-sm supply_calculate total_supply_cost">'+
                    '<span class="text-danger"><?php echo form_error('total_supply_cost');?></span>'+
            '</td>'+
            '<td>'+
            '<a href="#" style="z-index: 1000;"   class="btn btn-circle1 btn-danger pull-right remove_btn"><i class="fa fa-minus" aria-hidden="true"></i></span></a>'+'</td>'+
          '</tr>' ;
          //console.log(templets);
          
$(document).ready(function(){
     $(".click_btn").click(function(){
      //$("tbody").append(templets);
    var sno = ($(".add_line").find("tbody").find('tr').length)+1;
    $(".add_line").find("tbody").append(templets.replace(/{sno}/g, sno)); 
    $(".add_line").find("tbody").find("tr:last").find(".select2").select2(); 
   });
     <?php if(!$this->input->get('order') && !$this->input->post('auto_click_hide')){ ?>
         $(".click_btn").click();
       <?php } ?>
    $(document).on('click','.remove_btn',function(){
      // $(this).parents('tr').remove();
            var Tablerows = $(this).parents("tbody");
            if($(this).parents("tbody").find("tr").length>1){
               $(this).parents("tr").remove();
               Tablerows.find('tr').each(function(i,v){
                $(this).find('td:first').html(i+1);
               });
            }else{
             $(this).parents("tr").remove();
                       alert("You must have to one enter!");
             $(".click_btn").click();
            }
    });

    /*
      Spply calculation
    */
    $(document).on("blur",".supply_calculate",function(){
      
      var parentTr = $(this).parents('tr');
      var qty = parseFloat(parentTr.find("#supply_qty").val());
      var cost = parseFloat(parentTr.find("#supply_cost").val());
      if(!isNaN(qty) && !isNaN(cost)){
      parentTr.find("#total_supply_cost").val(qty*cost);
      }
      var sum = 0;
      $(".total_supply_cost").each(function(){
                 //sum += parseFloat(this.value);
                 sum += +$(this).val();
                //sum += this.value;
            });
      $("#total_payable").val(sum);
      //console.log(sum);balance_amt total_paid
       var paid = $("#total_paid").val();
       var  total =sum - paid;
       $("#balance_amt").val(total);
      // $("#total_paid").focusout(function () {
      //          var paid = parseFloat(this.value);
      //         var  total =sum - paid;
      //          $("#balance_amt").val(total);
      //                      });
    })
});
</script>
<script type="text/javascript">
//////////////////////////////////////////////////////////
///////////////////   for printable   ////////////////////
//////////////////////////////////////////////////////////
function printable(print){
            var prtContent = $("#"+print);
            var html = '<html><head><title>Print it!</title><link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>"><link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>"></head><body>'+prtContent.html()+'</body></html>';
            console.log(html);
            var WinPrint = window.open('', '', 'left=0,top=0,width=auto,height=auto,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(html);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
//////////////////////////////////////////////////////
//////////////  Add product formpost here   //////////
//////////////////////////////////////////////////////
$(document).ready(function(){
   $("#submit").click(function(){
    // check validation 
    var name            = $('#name').val(); 
    var cost            = $('#cost').val();
    var margin          = $('#margin').val();
    var price           = $('#price').val();
    var stock_keeping   = $('#stock_keeping').val();
    var product_quantity= $('#product_quantity').val();
    var stock_avilable  = $('#stock_avilable').val();
    var flag = true;
    /********validate all our form fields***********/
    /* Name field validation  */
    if(name==""){ 
        $('#name').css('border-color','red'); 
        flag = false;
    }else{
        $('#name').css('border-color','green');
        flag = true; 
    }
    /* email field validation  */
    
   
    //validation end here
    if(flag) 
    {
          $.ajax({
            type: 'POST',
            url: '<?php echo base_url('admin_area/modal_form_submit_by_ajax'); ?>',
            data: $('form').serialize(),
            dataType:'json',
            success: function (responce) {
              if(responce.status==200){
                var id=responce.data.id;
                var val=responce.data.value;
                var add_option='<option value="'+id+'">'+val+'</option>';
                 $(".add_line").find("tbody").find('select').append(add_option);
              }
              $("#myModal").modal('hide');
              alertify.success("Product Successfully Insert");
            }, 
            error: function () {
                  alertify.error("Sorry: You Are Un-Successfull");
              }
          });
    }

    });
   ///  PAID AMOUNT BY THE BALENCE---------
 $(document).on('click','.idss',function(){
        var id = $(this).attr('data-id');
        var bal = $(this).attr('data-balence');
        var supname = $(this).attr('data-supname');
        $('#our_modal').find("#find_stock_id").val(id);
        $('#our_modal').find("#balance_amt").val(bal);
        $('#our_modal').find("#supname").val(supname);
    });
});
</script>
  </body>
</html>
