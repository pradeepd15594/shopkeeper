<aside class="main-sidebar">
  <section class="sidebar"><!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/dashboard/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php $users = $this->session->userdata('user_data'); 
                    echo ucwords($users['name']);?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
      <ul class="sidebar-menu">
        <li class="header">HEADER</li>
        <li class="active">
          <a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-home"></i><span>Dashboard</span></a>
        </li>
        <!-- user creation page here -->
        <li class="<?php if(in_array($this->router->fetch_method(),array('add_user')) ){echo "active"; } ?>">
          <a href="#"><i class="fa fa-user-plus" aria-hidden="true"></i>
             <span>User Details</span><i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->router->fetch_method()=='add_user'){echo "active"; } ?>">
              <a href="<?php echo base_url()."add_user";?>">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Add User</span>
              </a>
            </li>
          </ul>
        </li>
        <!-- MEMBER DATA LIST ALL ITEM -->
         <li class="<?php if(in_array($this->router->fetch_method(),array('member_add','member_list')) ){echo "active"; } ?>">
          <a href="#"><i class="fa fa-truck" aria-hidden="true"></i>
             <span>Supplier Details</span><i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->router->fetch_method()=='add_member'){echo "active"; } ?>">
              <a href="<?php echo base_url()."add_member"; ?>">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Add Supplier</span>
              </a>
            </li>
            <li class="<?php if($this->router->fetch_method()=='member_list'){echo "active"; } ?>">
              <a href="<?php echo base_url()."member_list";?>">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Supplier List</span>
              </a>
            </li>
          </ul>
        </li>
         <!-- MEMBER DATA LIST ALL ITEM END HERE-->
        <!-- user creation page end here -->
        <li class="<?php if(in_array($this->router->fetch_method(),array('products','salse_uses')) ){echo "active"; } ?>">
          <a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i>
             <span>Product Details</span><i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->router->fetch_method()=='products'){echo "active"; }?>">
              <a href="<?php echo base_url()."products";?>">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Products</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="<?php if($this->router->fetch_method()=='stock'){echo "active"; } ?>">
          <a href="<?php echo base_url()."stock";?>">
            <i class="fa fa-cart-arrow-down" aria-hidden="true"></i><span>Purchase</span>
          </a>
        </li>
         <!-- all report here start  -->
          <li class="<?php if(in_array($this->router->fetch_method(),array('p-report', 'perchase-report', 'customer', 'sale_report')) ){echo "active"; } ?>">
          <a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i>
             <span>All Reports</span><i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->router->fetch_method()=='p-report'){echo "active"; } ?>">
              <a href="<?php echo base_url()."p-report"; ?>">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Product Report</span>
              </a>
            </li>
            <li class="<?php if($this->router->fetch_method()=='perchase-report'){echo "active"; } ?>">
              <a href="<?php echo base_url()."perchase-report"; ?>">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Purchase Report</span>
              </a>
            </li>
            <li class="<?php if($this->router->fetch_method()=='customer'){echo "active";}?>">
              <a href="<?php echo base_url()."customer"; ?>">
                  <i class="fa fa-circle" aria-hidden="true"></i><span>Customer Report</span>
              </a>
            </li>
            <li class="<?php if($this->router->fetch_method()=='sale_report'){echo "active";}?>">
              <a href="<?php echo base_url()."sale_report"; ?>">
                  <i class="fa fa-circle" aria-hidden="true"></i><span>Sales Report</span>
              </a>
            </li>
          </ul>
        </li>
         <li class="<?php if($this->router->fetch_method()=='setting'){echo "active"; } ?>">
              <a href="<?php echo base_url()."setting"; ?>">
                <i class="fa fa-cog" aria-hidden="true"></i><span>Setting</span>
              </a>
        </li>
        <li class="<?php if($this->router->fetch_method()=='setting'){echo "active"; } ?>">
              <a href="<?php echo base_url()."selling"; ?>">
                <i class="fa fa-scribd" aria-hidden="true"></i><span>Selling Panel</span>
              </a>
        </li>
        
         <!-- all report here end  -->

    </ul><!-- /.sidebar-menu -->
  </section>
<!-- /.sidebar -->
</aside>