<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><i class="fa fa-th" aria-hidden="true"></i>&nbsp;New User</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url("admin_area"); ?>"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active"> Add User</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
         <div class="box box-success">
         <?php if($this->input->get('add')){ ?>
         <div class="box-header ui-sortable-handle">
           <a href="<?php echo base_url('add_user'); ?>" style="margin: 1px;" class="btn btn-success pull-right">User List</a>
         </div>
           <div class="content">
             <form id="course-form" method="post"  enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group field-batches-start_date required">
                            <label class="control-label" for="batches-start_date">User Type:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                             <select class="form-control gallery_change" name="user_type">
                                <option value="">Select Your Area</option>
                                    <option value="admin" <?php if(set_value('user_type')=='admin'){ echo 'selected';}?> <?php if(isset($editItem) && $editItem['user_type'] =='admin'){ echo "selected";  } ?>>Admin</option>
                                    <option value="staff" <?php if(set_value('user_type')=='staff'){ echo 'selected';}?> <?php if(isset($editItem) && $editItem['user_type'] =='staff'){ echo "selected";  } ?> >Staff</option>
                              </select>
                            <span class="text-danger"><?php echo form_error('user_type');  ?></span>
                        </div> 
                    </div>
                    <div class="col-sm-4 col-xs-4">
                      <div class="form-group field-batches-start_date required">
                        <label class="control-label" for="batches-start_date">User Name:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                          <input type="text"  class="form-control" name="user_name"
                                 value='<?=set_value('user_name',@$editItem['user_name']);?>'>
                          <span class="text-danger"><?php echo form_error('user_name');  ?></span>
                      </div> 
                    </div>
                    <div class="col-sm-4 col-xs-4">
                      <div class="form-group field-batches-batch_name ">
                        <label class="control-label" for="batches-start_date">Password:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                          <input type="text"  class="form-control" name="password"
                                 value='<?=set_value('password',@$editItem['password']);?>'>
                          <span class="text-danger"><?php echo form_error('password');  ?></span>
                      </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                      <div class="form-group field-batches-start_date required">
                        <label class="control-label" for="batches-start_date">Mobile No:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                          <input type="text" class="form-control" maxlength="10" onkeyup="onlynumeric(this)"; name="mobile"
                                 value='<?=set_value('mobile',@$editItem['mobile']); ?>'>
                          <span class="text-danger"><?php echo form_error('mobile'); ?></span>
                        </div> 
                    </div>
                    <div class="col-sm-4 col-xs-4">
                      <div class="form-group field-batches-start_date required">
                        <label class="control-label" for="batches-start_date">User Detail:&nbsp;&nbsp;<i class="text-danger">*</i></label>
                          <input type="text" class="form-control" name="detail"
                                 value='<?=set_value('detail',@$editItem['detail']); ?>'>
                          <span class="text-danger"><?php echo form_error('detail'); ?></span>
                        </div> 
                    </div>
                 </div>
                <div class="row">
                  <div class="col-sm-4 col-xs-4">
                    <label class="control-label"><hr></label>
                      <button type="submit" class="btn btn-primary btn-create">SUBMIT</button> &nbsp;&nbsp;&nbsp;&nbsp;<button type="reset" name="reset" class="btn btn-danger btn-create">CANCEL</button>
                  </div>
                </div>
          </form>
          <!-- category listing start -->
          <hr>
          <?php }else{ ?>
          <div class="box-header ui-sortable-handle">
           <h4><?php if($this->input->get('view_user')){ echo 'All User';}?><a href="<?php echo base_url('add_user?add='); ?>1" class="btn btn-success pull-right">Add New User</a></h4>
         </div>
          <div class="box-body">
          <div class="row">
                    <div id="printable" class="col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-striped table-bordered table-condensed table-hover" style="margin-bottom:5px;">
                            <thead>
                                <tr class="active" role="row">
                                    <th>S.No</th>
                                    <th>User Name</th>
                                    <th>User Type</th>
                                    <th>User Mobile</th>
                                    <th>User Details</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php $i=1;  if(isset($list_item)){
                                foreach($list_item as $row){
                             ?>
                                <tr>
                                    <td><?php echo $i;$i++; ?></td>                                    
                                    <td><?=ucwords($row['user_name']); ?></td>
                                    <td><?php echo ucwords($row['user_type']);?></td>
                                    <td><?php echo ucwords($row['mobile']);?></td>
                                    <td><?php echo ucwords($row['detail']);?></td>
                                    <td>
                                       <a href="#" id="confirm" data-id="<?php echo base_url('add_user?trash='.$row['id']); ?>" class="tip btn btn-danger btn-xs confirm"><i class="fa fa-trash-o"></i></a>
                                       <a href="<?php echo base_url('add_user?add='.$row['id'].'&up_id='.$row['id']); ?>" class="tip btn btn-success btn-xs"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                      <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
             <?php } ?>
             </div>
             </div>
          <!-- category listing End -->
         </div>
          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 