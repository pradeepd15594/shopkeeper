<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_area extends CI_Controller {
 function __construct(){
        parent::__construct();     
        $this->load->library('upload');
        if($this->session->userdata('user_data')==null){ redirect(base_url('login/logout'));}
    }
 
/////////////////////////////////////////
///////////   after login page    ///////  
/////////////////////////////////////////
public function index($message = NULL){              
        $this->dashboard();
} 
/////////////////////////////////////////
//////////   Dashboard Page    //////////
/////////////////////////////////////////
    public function dashboard(){
        $data['title']="Dashboard";
        $date=date('Y-m-d');
        $data['today_sale'] = $this->db->query("SELECT  sum(total_amount) as `total_amount` FROM `purchase_product` WHERE date(created_at) = '$date'")->row_array();
       $data['stock_msg'] = $this->db->query("SELECT * FROM `products` WHERE `quantity`<=`reorder_point` ORDER BY `name` ASC")->result_array();

        $this->render_view('dashboard',$data);
    }

/////////////////////////////////////////
//////////   Category Page     //////////
/////////////////////////////////////////
    public function product(){
        $data['title']="Category";
        if($this->input->get('pro_trash')){
            $this->db->where('id', $this->input->get('pro_trash'));
            $this->db->delete('products');
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Delete Successfully '));
            redirect(base_url('products'));
        }
        if($this->input->get('product_up')){
          $UID=$this->input->get('product_up');
          $data['editItem']= $this->db->query("SELECT * from products where `id` = '$UID'")->row_array();
        }
        if($this->input->server('REQUEST_METHOD')=="POST"){
            $this->form_validation->set_rules('name', 'Category Name', 'required');
            $this->form_validation->set_rules('product_code', 'Product Code','trim|required');
            $this->form_validation->set_rules('reorder_point','Reorder Point','trim|required');
            $this->form_validation->set_rules('category_type','Category Type','trim|required');
            $this->form_validation->set_rules('cost', 'Titles', 'trim|required');
            $this->form_validation->set_rules('product_quantity','User Quantity', 'required');
            if($this->form_validation->run() ==  TRUE ){
                $data=array(
                            'name'=>$this->input->post('name'),
                            'product_code'=>$this->input->post('product_code'),
                            'cost'=>$this->input->post('cost'),
                            'total_price'=>$this->input->post('cost')*$this->input->post('product_quantity'),
                            'quantity'=>$this->input->post('product_quantity'),
                            'reorder_point' => $this->input->post('reorder_point'),
                            'user_id'=>$this->input->post('user_id'),
                            'created_at' => date('d-m-Y H:i:s'),
                            'updated_at' => date('d-m-Y H:i:s')
                           );
                if($this->input->get('product_up')){
                    $this->db->where('id', $this->input->get('product_up'));
                    $Up_response=$this->db->update('products', $data);
                }else{
                    $response=$this->db->insert('products',$data); 
                }
                // flash message show here
                if($response){
                    $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Insert'));
                }elseif($Up_response){
                    $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Update'));
                }else{
                    $this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>' Some error occurred. Please try again.'));
                }redirect('products');
            }//If closed validation true
            }//If closed Post
        $data['list_item'] = $this->db->query("SELECT * FROM `products`")->result_array();
        $data['product_name'] = $this->db->query("SELECT * FROM `product_category`")->result_array();
        $this->render_view('product',$data);
     }
//////////////////////////////////////////
///////////  Stock Management    /////////
//////////////////////////////////////////
    public function stock(){
        if($this->input->server('REQUEST_METHOD')=="POST"){
            $this->form_validation->set_rules('supl_id', 'Supplier  Name', 'required');
            $this->form_validation->set_rules('supl_invoice', 'Supplier Name', 'required');
            $this->form_validation->set_rules('due_date', 'Due Date', 'required|callback_date_check');
            $error=0;$data['errormsg'] = "";
            $supl_productchk = count(array_filter($this->input->post('product_id')));
            $supply_qtychk = count(array_filter($this->input->post('supply_qty')));
            $supply_costchk = count(array_filter($this->input->post('supply_cost')));
            if($supl_productchk != $supply_qtychk || $supply_qtychk != $supply_costchk || $supply_costchk != $supl_productchk){
                $error=1;
                $data['errormsg'] = "please Check below data yourself, product section has not contains appropriate data rectify its first.";
            }
            $this->form_validation->set_rules('total_supply_cost', 'Total Supply Cost', '');
            $this->form_validation->set_rules('payable_amt', 'Total Payable', 'required');
            $this->form_validation->set_rules('paid_amt', 'Total Paid', 'required');
            $this->form_validation->set_rules('balance_amt', 'Balance', 'required');
            $this->form_validation->set_rules('pay_mode', 'Payment Mode', 'required');
            if($this->input->post('pay_mode')=='Cheque'){
                $this->form_validation->set_rules('cheque_no', ' Cheque Number','trim|required'); 
            }elseif($this->input->post('pay_mode')=='NEFT'){
                $this->form_validation->set_rules('neft_no', 'NEFT Unique Trasaction Number','trim|required');
            }elseif($this->input->post('pay_mode')=='RTGS'){
                $this->form_validation->set_rules('rtgs_no','RTGS Unique Trasaction Number','trim|required');
            }
            //when error occured
            $data['updateItem'] = $this->input->post();
                $supl_product = $this->input->post("product_id");
                $supply_qty = $this->input->post("supply_qty");
                $supply_cost = $this->input->post("supply_cost");
                $total_supply_cost = $this->input->post("total_supply_cost");
            
            for($k =0; $k<count($this->input->post("product_id")); $k++){
                $data['updateItem']['up_pruduct'][] = array('product_description'=>$supl_product[$k],'qty'=>$supply_qty[$k],'price'=>$supply_cost[$k],'amount'=>$total_supply_cost[$k]);
            }
            
            if($this->form_validation->run() == TRUE && $error==0){
                $this->db->trans_begin();
                $due_date = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post("due_date"))));;
                $data =array(
                           'supl_id' => $this->input->post("supl_id"),
                           'supl_invoice' =>$this->input->post("supl_invoice"),
                           'due_date' => $due_date,
                           'payable_amt' =>$this->input->post("payable_amt"),
                           'paid_amt' => $this->input->post("paid_amt"),
                           'balance_amt' =>$this->input->post("balance_amt"),
                           'pay_mode' => $this->input->post("pay_mode"),
                           'neft_tr_id' =>$this->input->post("neft_no"),
                           'rtgs_tr_id' =>$this->input->post("rtgs_no"),
                           'cheque_no' => $this->input->post("cheque_no"),
                           'order_date' => date('Y-m-d H:i:s')
                          );
                if($this->input->get("order")){// On update
                    $id  = $this->input->get("order");
                    $this->db->where('id',$id);
                    $response = $this->db->update('stock_control', $data);//update in product table
                    $stocks = $this->db->query("SELECT `product_description`, sum(qty) as `qtys` FROM `stock_product` WHERE `order_no`='$id' GROUP BY `product_description`")->result_array();
                    foreach ($stocks as $row){
                         $this->db->where('id', $row['product_description']);
                         $this->db->set('avilable', 'avilable-'.$row['qtys'], FALSE);
                         $this->db->update('products');
                    }
                    $this->db->where('order_no', $id)->delete('stock_product');
                }else{
                    $response = $this->db->insert('stock_control', $data);
                    $id = $this->db->insert_id(); 
                }
                //Product insert Of Curret order
                //Product insert Of Curret order
                $supl_product = $this->input->post("product_id");
                $supply_cost = $this->input->post("supply_cost");
                $supply_qty = $this->input->post("supply_qty");

                    for($i=0; $i<count($this->input->post("product_id")); $i++){
                        $this->db->where('id', $supl_product[$i]);
                        $this->db->set('avilable', 'avilable+'.$supply_qty[$i], FALSE);
                        $this->db->update('products');
                    } 
                for($i=0; $i<count($this->input->post("product_id")); $i++){
                    if($supl_product[$i]){
                        $datas[]=array(
                               'order_no' =>$id,
                               'product_description' =>$supl_product[$i],
                               'price' => $supply_cost[$i],
                               'qty' => $supply_qty[$i],  
                               'amount' =>$supply_cost[$i]*$supply_qty[$i],
                               'created_at' => date('Y-m-d H:i:s'),
                               'updated_at' => date('Y-m-d H:i:s')
                              );
                    }
                } 
                $this->db->insert_batch('stock_product', $datas);
                                $invoiceID = $this->generateInvoice();
                if ($this->input->post('supl_id')){
                    $debit=array(
                               'member_id' =>$this->input->post('supl_id'),
                               'amount' =>$this->input->post('payable_amt'),
                               'remark' =>'Amount due for'.$invoiceID,
                               'purpose' => 'debit',
                               'created_at' => date('Y-m-d H:i:s'),
                               'updated_at' => date('Y-m-d H:i:s')
                              );
                   $this->db->insert('member_account', $debit);
                }
                if($this->input->post('supl_id')){
                    $credit=array(
                               'member_id' =>$this->input->post('supl_id'),
                               'amount' =>$this->input->post('paid_amt'),
                               'remark' =>'Amount due for'.$invoiceID,
                               'purpose' => 'credit',
                               'created_at' => date('Y-m-d H:i:s'),
                               'updated_at' => date('Y-m-d H:i:s')
                              );
                   $this->db->insert('member_account', $credit);
                    
                }
                if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>' Some error occurred. Please try again.'));
                }else{
                    $this->db->trans_commit();
                    $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Insert'));
                  redirect(base_url('stock'));
                }//transe status loop close here
            }   
    }//post close here
    if($this->input->get("order") && !isset($data['updateItem'])){// On update
        $stock_Id  = $this->input->get("order");
        $data['updateItem'] = $this->db->get_where("stock_control",array('id'=>$stock_Id))->row_array();
        $data['updateItem']['up_pruduct'] = $this->db->get_where("stock_product",array('order_no'=>$stock_Id))->result_array();
    }
    if($this->input->get('close')){
        $close_id = $this->input->get('close');
        $data=array('is_close' => 'close');
        $this->db->where('id',$close_id);
        $response = $this->db->update('stock_control', $data);
        redirect('stock');
    }
    $data['title']="Stock control";
    if($this->input->get("fromdate") && $this->input->get("todate")){
        $fromdate=date('Y-m-d H:i:s',strtotime($this->input->get("fromdate")));
        $todate=date('Y-m-d H:i:s',strtotime($this->input->get("todate")));
        $data['stock_list'] = $this->db->query("SELECT C.*, D.name FROM stock_control as C left join members as D ON C.supl_id=D.id WHERE order_date >='$fromdate' AND order_date <= '$todate'")->result_array();
    }elseif($this->input->get("membername")){
        $memid=$this->input->get("membername");
        $data['stock_list'] = $this->db->query("SELECT C.*, D.name FROM stock_control as C left join members as D ON C.supl_id=D.id WHERE `supl_id`='$memid'")->result_array();
    }else{
        $data['stock_list'] = $this->db->query("SELECT C.*, D.name  FROM stock_control as C left join members as D ON C.supl_id=D.id")->result_array();   
    }
    //print_r($data['stock_list']);die;
    $data['vendors'] = $this->db->query("SELECT * FROM `members` where `name` != ''")->result_array();
    $data['product'] = $this->db->query("SELECT * FROM `products` where `name` != ''")->result_array();
      $this->render_view('stock',$data);
    }
//due date check
public function date_check($due_date){
         $end_date = date("Y-m-d H:i:s");
         $due_date = date('Y-m-d H:i:s', strtotime($due_date));
              if($end_date < $due_date){ 
                  $this->form_validation->set_message('date_check', 'The %s  are less than Today Date ');
                   return FALSE;
               }else{
                   return TRUE;
                  
               } 
         }
// form validation
 public function validates($a,$k){
  if(is_array($a)){
      foreach ($a as $key => $value){
        if($value==''){ 
          $this->errors[$k][$key] = "This Field required";
          return true;
        }
      }
   }else{
       return TRUE; 
   } 
   return TRUE;
}


/////////////////////////////////////////
//////////     Member details    ////////
/////////////////////////////////////////
public function member_add(){
    if($this->input->server('REQUEST_METHOD')=="POST"){
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|trim|stripslashes');
        $this->form_validation->set_rules('address','Address','required|xss_clean|trim|stripslashes');
        $this->form_validation->set_rules('contact','Contact No','required|xss_clean|trim|stripslashes|numeric');
        
        if($this->form_validation->run()==TRUE){
            $date = date_create(date("Y-m-d H:i:s"));
            $member_id = $this->input->post("member_id");     
            $data = array(
                        'name' =>$this->input->post('name'),
                        'contact_no' =>$this->input->post('contact'),
                        'address' => $this->input->post('address'),
                        );
            $response = $this->db->insert('members', $data);
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Member Data Saved Successfully.'));
            redirect(base_url()."member_list");
        }
    }
    if($this->input->get('m_up') && is_numeric($this->input->get('m_up'))){
        $id = $this->input->get('m_up');
        $data['preData']=$this->db->query("SELECT * FROM `members` where `id`= '$id' ")->row_array();
        $data['section'] = "Update";
    }else{
        $data['section'] = "Add New";
    }
    $data['title'] = "Supplier";
    $this->render_view('member_add',$data);
}//member alll listing...
public function member_list(){
    $data['title'] = "Supplier";
    $data['section'] = "List";
    if($this->input->get('membername')){
        $mid=$this->input->get('membername');
        $data['members']=$this->db->query("SELECT * FROM `members` Where `id`='$mid'")->result_array();
    }else{
        $data['members']=$this->db->query("SELECT * FROM `members`")->result_array();
    }
    $data['members_all']=$this->db->query("SELECT * FROM `members` ORDER BY `created_on` DESC")->result_array();
    
    $this->render_view('member_list',$data);
}
    
//////////////////////////////////////////
//////////   Member Account    ///////////
//////////////////////////////////////////
public function members_accounts(){
    $id = $this->input->get('account');
    if($this->input->server("REQUEST_METHOD")=='POST'){
        $this->form_validation->set_rules('amount', 'Amount', 'required|xss_clean|trim|stripslashes');
        $this->form_validation->set_rules('remark', 'Remark', 'required|xss_clean|trim|stripslashes');
        $this->form_validation->set_rules('purpose', 'Purpose', 'required');
        if($this->form_validation->run()==TRUE){

             $data = array(
                        'member_id' =>$this->input->get('account'),
                        'amount' =>$this->input->post('amount'),
                        'remark' => $this->input->post('remark'),
                        'purpose' => $this->input->post('purpose'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                        );
            $response = $this->db->insert('member_account', $data);
            if($response){
                $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>' Successfully Saved.'));
            }
                redirect(base_url()."member-accounts?account=".$id);
            }
         } 
        if($this->input->get('account')){
          $data['membersDetails']=$members=$this->db->query("SELECT * FROM `members` Where `id`='$id'")->row_array(); 
          $data['member_account_lists']=$this->db->query("SELECT * FROM `member_account` WHERE member_id='$id' ORDER BY `created_at` DESC")->result_array();
        }
        $data['credit_amount'] = $this->db->query("SELECT SUM(amount) as credit_amount from member_account where `purpose`= 'credit' and `member_id`='$id'")->row_array();
        $data['debit_amount'] = $this->db->query("SELECT SUM(amount) as debit_amount from member_account where `purpose`= 'debit' and `member_id`='$id'")->row_array();
        $data['form_data']=$this->db->query("SELECT * FROM `member_account` Where `member_id`='$id'")->row_array();
        $data['title'] = "Supplier";
        $data['section'] = "Acounts";
        if($this->input->get('fromdate') && $this->input->get('todate') && $this->input->get('account')){
              $ids = $this->input->get('account');
              $fromdate=date('Y-m-d',strtotime($this->input->get("fromdate")));
              $todate=date('Y-m-d',strtotime($this->input->get("todate")));
              $data['member_account_lists']= $this->db->query("SELECT * FROM `member_account` WHERE created_at >='$fromdate' AND created_at <= '$todate' AND member_id='$ids' ORDER BY `member_id` DESC")->result_array();
        }
        $this->render_view('members_accounts',$data);
}
//////////////////////////////////////////
//////////        add_user     ///////////
//////////////////////////////////////////
public function add_user(){
    $data['title'] = 'New Users';
    if($this->input->get('trash')){
        $this->db->where('id', $this->input->get('trash'));
        $this->db->delete('user');
        $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Delete Successfully '));
        redirect(base_url('add_user'));
    }
    if($this->input->get('up_id')){
        $EID=$this->input->get('up_id');
        $data['editItem'] = $this->db->query("SELECT * from user where `id` = '$EID'")->row_array();
    }
     if($this->input->server('REQUEST_METHOD')=="POST"){
        $this->form_validation->set_rules('user_type', 'User Types', 'required');
        $this->form_validation->set_rules('user_name', 'User Name ', 'required');
        $this->form_validation->set_rules('mobile', 'Mobile ', 'required');
        $this->form_validation->set_rules('password', 'User Password ', 'required');
        $this->form_validation->set_rules('detail', 'User Detail ', 'required');
        $this->form_validation->set_rules('detail', 'User Detail ', 'required');
        if($this->form_validation->run() == TRUE){
            $data = array(
                         'name' => $this->input->post('user_name'),
                         'user_name' => $this->input->post('user_name'),
                         'user_type' => $this->input->post('user_type'),  
                         'mobile' => $this->input->post('mobile'),  
                         'password' => $this->input->post('password'),  
                         'detail' => $this->input->post('detail'), 
                         'created_at' => date('Y-m-d H:i:s'),
                         'updated_at' => date('Y-m-d H:i:s'),
                        );
            if($this->input->get('up_id')){
                $this->db->where('id', $this->input->get('up_id'));
                $Up_response = $this->db->update('user', $data);
            }else{
                $response = $this->db->insert('user',$data); 
            }
            if($response){
                $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Insert'));
            }elseif($Up_response){
                $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Update'));
            }else{
                $this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>' Some error occurred. Please try again.'));
            }
                redirect('add_user');
            }//FORM VALDATION
        }//POST END
    $data['list_item']=$this->db->query("SELECT * FROM `user`")->result_array();
    $this->render_view('add_user',$data);
}
//////////////////////////////////////////
//////////   PRODUCT REPORTS   ///////////
//////////////////////////////////////////
public function product_report(){
    $data['title'] = 'Product Report ';
    if($this->input->get("fromdate") && $this->input->get("todate")){
    $fromdate=date('Y-m-d H:i:s',strtotime($this->input->get("fromdate")));
    $todate=date('Y-m-d H:i:s',strtotime($this->input->get("todate")));
    $data['all_data']= $this->db->query("SELECT * FROM `products` WHERE created_at >='$fromdate' AND created_at <= '$todate' ORDER BY `product_code` DESC")->result_array();
    }else{
        $data['all_data']= $this->db->query("SELECT * FROM `products` ORDER BY `product_code` DESC")->result_array();
    }
    $this->render_view('product_report',$data);
}
//////////////////////////////////////////
//////////    perchase_report  ///////////
//////////////////////////////////////////
public function perchase_report($limit=0){
    $data['title'] = 'Perchase Report';
    if($this->input->get("fromdate") && $this->input->get("todate")){//when search by the date
        $fromdate=date('Y-m-d H:i:s',strtotime($this->input->get("fromdate")));
        $todate=date('Y-m-d H:i:s',strtotime($this->input->get("todate")));
        $q="SELECT C.*, D.name FROM stock_control as C left join members as D ON C.supl_id=D.id WHERE order_date >='$fromdate' AND order_date <= '$todate'";
    }else{// it show by default
        $q="SELECT C.*, D.name  FROM stock_control as C left join members as D ON C.supl_id=D.id";
    }

    $this->load->library('pagination');
    $config['base_url'] = base_url('perchase-report');
    $config['total_rows'] = $this->db->query($q)->num_rows();
    $config['per_page'] = 10;
    $config["uri_segment"] = 2;
    $data['limit']=$limit;
    //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
    $data['stock_list']=$this->db->query("$q LIMIT $limit, ".$config['per_page'])->result_array();
    $this->pagination->initialize($config);
    $data['link'] = $this->pagination->create_links();
    ///..............pagination............
    $this->render_view('perchase_report',$data);
}
//////////////////////////////////////////
//////////     contact_form    ///////////
//////////////////////////////////////////
    public function contact_form(){
        $data['title'] = 'Contact Us';
        $data['contact_list']=$this->db->query("SELECT * FROM `contact_us`")->result_array();
        $this->render_view('contact_form',$data);
    }
///////////////////////////////////////// 
////////     change Password     ////////
/////////////////////////////////////////
public function change_password(){
    $data='';
    $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
    $this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[re_password]');
    $this->form_validation->set_rules('re_password', 'Retype Password', 'required|trim');
    if($this->form_validation->run() == TRUE){
        $sessionData = $this->session->userdata('user_data');
        $this->data['id'] = $id = $sessionData['id'];
        $this->data['name'] = $sessionData['name'];
        $password = $this->input->post('newpassword');
        $old_password = $this->input->post('old_password');
        $sql=$this->db->get_where("user", array("id"=>$id,"password"=>$old_password) )->result_array();
        if(count($sql)>0){
            $this->db->where('id', $id);
            $this->db->update("user",array('password'=>$password));
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Password Successfully Change'));
        }else{
               $data['error_msg'] = "Old  Passsword did not match";  
             }
    }
    $this->render_view('change_password',$data);
}

////////////////////////////////////////////
////////    product Category   /////////////
////////////////////////////////////////////
public function category($limit=0){
    if($this->input->get('cdel')){
        $this->db->where('id', $this->input->get('cdel'));
        $this->db->delete('product_category');
        $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Delete Successfully '));
        redirect(base_url('category'));
    }
    // COTEGORY POST HERE--
    if($this->input->server('REQUEST_METHOD')=="POST"){
        $this->form_validation->set_rules('medicine', 'Category Name ', 'required|trim');
        if($this->form_validation->run() == TRUE){
            $data=array('category'=> $this->input->post('medicine'));
            $res = $this->db->insert('product_category',$data); 
            if($res){
                $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Insert'));
            }else{
                $this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>'Failed : Please Try Again'));
            }
            redirect('category');
        }
    }
    // PAGINATION START HERE--
  $data['title'] = "Category";
    $q="SELECT * FROM `product_category`";
    $this->load->library('pagination');
    $config['base_url'] = base_url('category');
    $config['total_rows'] = $this->db->query($q)->num_rows();
    $config['per_page'] = 12; 
    $config["uri_segment"] = 2;
    $data['limit']=$limit;
    //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

    $data['medicine']=$this->db->query("$q LIMIT $limit, ".$config['per_page'] )->result_array();
    $this->pagination->initialize($config);
    $data['link'] = $this->pagination->create_links();
    $this->render_view('category',$data);
}
////////////////////////////////////////////
////////   PERCHASE REPORT  ////////////////
/////////////////////////////////////////////
public function customer_perchase_report($limit=0){
  $data['title'] = "Purchase Report";
    $data['section'] = "List";
        if($this->input->get('membername')){ // when search by the member name
        $mid=$this->input->get('membername');
        $q="SELECT * FROM `members` Where `id`='$mid'";
    }else{// it show by default
        $q="SELECT * FROM `purchase_order` ORDER BY `date` DESC";
    }

    $this->load->library('pagination');
    $config['base_url'] = base_url('customer');
    $config['total_rows'] = $this->db->query($q)->num_rows();
    $config['per_page'] = 8; 
    $config["uri_segment"] = 2;
    $data['limit']=$limit;
    //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

    $data['customer']=$this->db->query("$q LIMIT $limit, ".$config['per_page'] )->result_array();
    $this->pagination->initialize($config);
    $data['link'] = $this->pagination->create_links();
    $this->render_view('customer_perchase_report',$data);
}
//////////////////////////////////////////
//////////    setting page     ///////////
//////////////////////////////////////////
public function setting(){
    $data['title'] = 'Perchase Report';
        $this->form_validation->set_rules('store_name', 'Store Name', 'trim');
        $this->form_validation->set_rules('shop_no', 'Shop No', 'trim');
        $this->form_validation->set_rules('tel_no', 'Phone No', 'trim');
        $this->form_validation->set_rules('address', 'Address', 'trim');
        $this->form_validation->set_rules('tax_include', 'Tax Include', 'trim');
        if($this->form_validation->run()==TRUE){
          $id=$this->input->post('id');
             $data = array(
                        'store_name' =>$this->input->post('store_name'),
                        'shop_no' =>$this->input->post('shop_no'),
                        'tel_no' => $this->input->post('tel_no'),
                        'address' => $this->input->post('address'),
                        'tax_include' => $this->input->post('tax_include'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                        );
        if($this->input->post("id")){
          $this->db->where('id',$id);
          $this->db->update('setting', $data);
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Update Successfully.'));
        }elseif(!$this->input->post("id")){
            $response = $this->db->insert('setting', $data);
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Insert Successfully.'));
        }
      }
      $data['editItem'] = $this->db->query("select * from setting")->row_array(); 
      $this->render_view('setting',$data);    
   }
   ////////////////////////////////////////////
////////      Salse REPORT  ////////////////
////////////////////////////////////////////
public function sales_report($limit=0){
  $data['title'] = "Sales Report";
    $data['section'] = "List";
    if($this->input->get("fromdate") && $this->input->get("todate")){//when search by the date
        $fromdate=date('Y-m-d H:i:s',strtotime($this->input->get("fromdate")));
        $todate=date('Y-m-d H:i:s',strtotime($this->input->get("todate")));
        $q="SELECT * FROM `purchase_order` WHERE date >='$fromdate' AND date <= '$todate'";
    }elseif($this->input->get('paid_by')){ // when search by the member name
        $pid=$this->input->get('paid_by');
        $q="SELECT * FROM `purchase_order` Where `paidby`='$pid'";
    }else{// it show by default
        $q="SELECT * FROM `purchase_order` ORDER BY `date` DESC";
    }

    $this->load->library('pagination');
    $config['base_url'] = base_url('sale_report');
    $config['total_rows'] = $this->db->query($q)->num_rows();
    $config['per_page'] = 10; 
    $config["uri_segment"] = 2;
    $data['limit']=$limit;
    //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

    $data['customer']=$this->db->query("$q LIMIT $limit, ".$config['per_page'] )->result_array();
    $this->pagination->initialize($config);
    $data['link'] = $this->pagination->create_links();
    $this->render_view('sales_report',$data);
}

/////////////////////////////////////////////////////////////
//////////  Header, Footer, Sidebar are includes   //////////
/////////////////////////////////////////////////////////////
    private function render_view($view,$data){
        $this->load->view('admin_includes/header',$data);
        $this->load->view('admin_includes/sidebar',$data);
        $this->load->view($view,$data);
        $this->load->view('admin_includes/footer',$data);
    }
///////////////////////////////////////////////
///////////////// pays_amount   ///////////////
///////////////////////////////////////////////
public function pays_amount(){
  if($this->input->server("REQUEST_METHOD")=='POST'){
      $pay = $this->input->post('pays_amount');
      $pays_id = $this->input->post('pays_id');
        if(count($pay)>0 && count($pays_id)>0){
            $data= $this->db->query("SELECT paid_amt,balance_amt FROM `stock_control` where `id`='$pays_id'")->result_array();
            
            foreach ($data as $row) {
               $p=$row['paid_amt'];           
               $b=$row['balance_amt'];           
             }  
             $paids=array(
                 'paid_amt'=> $p+$pay,
                 'balance_amt'=>$b-$pay
              );
          $this->db->where('id', $pays_id);
          $suc =$this->db->update('stock_control',$paids);
          if($this->input->post('supname')){
                    $credit=array(
                               'member_id' =>$this->input->post('supname'),
                               'amount' =>$pay,
                               'remark' =>'Amount due for-'.$this->generateInvoice(),
                               'purpose' => 'credit',
                               'created_at' => date('Y-m-d H:i:s'),
                               'updated_at' => date('Y-m-d H:i:s')
                              );
                   $this->db->insert('member_account', $credit); 
                }
          if(count($suc)>0){
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>' Transaction Successfully'));
          }else{ $this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>' Transaction Failed'));}
          redirect('stock');
        }
  }
}
////////////////////////////////////////////
//////////    genrate invoice     //////////
////////////////////////////////////////////
    // function for invoice id
    private function generateInvoice(){     
        $stamp = date("dmY");
        $mt = microtime();
        $mt = str_replace(" ", "", $mt);
        $mt = str_replace(".", "", $mt);
        $mt = substr($mt,0,6);      
        return $stamp.$mt;
    }

//calling form submet by ajax:
    public function modal_form_submit_by_ajax(){
        $this->form_validation->set_rules('name', ' name', 'required');
            if($this->form_validation->run() == TRUE){
                $data = array(
                             'name' => $this->input->post('name'),
                             'product_code' => 0,
                             'cost' => 0, 
                             'total_price' => 0, 
                             'quantity' => 0,  
                             'created_at' => date('Y-m-d H:i:s'),
                             'updated_at' => date('Y-m-d H:i:s'),
                            );
                 $this->db->insert('products',$data);
                  $id = $this->db->insert_id();
                  $option = $this->db->query("select * from products where `id`='$id'")->row_array();
                echo json_encode(array('status'=>200, 'data'=>array('id'=>$option['id'], 'value' =>$option['name'])));
                
        }
    }
} ?>