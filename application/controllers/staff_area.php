<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Staff_area extends CI_Controller {
	public $sessionData;
    function __construct(){
        parent::__construct();
    	$this->load->library('upload');
    		if($this->session->userdata('user_data')==null){
				redirect(base_url('login/logout'));
			}    
			$this->sessionData = $this->session->userdata('user_data');
    }
/////////////////////////////////////////
///////////   after login page    ///////  
/////////////////////////////////////////
	public function index($message = NULL){              
	    $this->selling();
	} 
	public function selling(){
		$datas=array();
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$this->db->trans_begin();
				    $invoiceID = $this->generateInvoice();
                    $data=array(
							 'invoice_type'=>$this->input->post('invoice_type'),
							 'invoice_id'=> $invoiceID,
							 'customer'=>$this->input->post('cust_name'),
							 'customer_phone'=>$this->input->post('cust_phone'),
							 'address'=>$this->input->post('address'),
							 'tax'=>$this->input->post('tax'),
							 'discount'=>$this->input->post('discount'),
							 'paidby'=>$this->input->post('paid_by'),
							 'payable_amt'=>$this->input->post('grandtotal'),
							 'paid'=>$this->input->post('paid'),
							 'cheque'=>$this->input->post('cheque'),
							 'cheque_no'=>$this->input->post('card_no'),
							 'cc_holder_val'=>$this->input->post('credit_holder'),
							 'date'=>date('Y-m-d H:i:s')
             	        ); 
                
             $this->db->insert('purchase_order', $data);
             /// insert valur in cudtomer table here-
             $data1=array(
						 'name'=>$this->input->post('cust_name'),
						 'phone'=>$this->input->post('customer_phone'),
						 'address'=>$this->input->post('address'),
						 'date_added'=>date('Y-m-d H:i:s')
             	        );
             $this->db->insert('customers', $data1);
             // MULTIPLE PRODUCT ......
                    $pqty = $this->input->post('pqty');
                    $price = $this->input->post('price');
                    $id = $this->input->post('all_pro_id');
                    $cust_name = $this->input->post('cust_name');
	                for($i=0; $i<count($this->input->post('all_pro_id')); $i++){
	                    if($cust_name[$i]){
	                        $datas[]=array(
		                               'customer_name' =>$cust_name,
		                               'product_id' =>$id[$i],
		                               'invoice_id' =>$invoiceID,
		                               'qty' =>$pqty[$i],
		                               'price' => $price[$i], 
		                               'total_amount' =>$pqty[$i]*$price[$i],
		                               'created_at' => date('Y-m-d H:i:s')
	                                   );
	                    }
	                }
	                //print_r($datas);die;
	                $this->db->insert_batch('purchase_product',$datas);
	       // }//CERTIFICATE UOLOAD OF DOCTORS
            // perchase stock when...
                    for($i=0; $i<count($this->input->post('all_pro_id')); $i++){
                        $this->db->where('id', $id[$i]);
                        $this->db->set('quantity', 'quantity-'.$pqty[$i], FALSE);
                        $this->db->update('products');
                    }
                 if($this->db->trans_status() == FALSE){
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>'Some error occurred. Please try again.'));
                }else{
                    $this->db->trans_commit();
                    $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Insert'));
                    redirect(base_url('billing?id='.$invoiceID));
                }//transe status loop close here
	}
		$data['all_product']=$this->db->query("SELECT * FROM `products` ORDER BY id desc")->result_array();
        $data['tax'] = $this->db->query("select * from setting")->row_array();
		$this->load->view("selling", $data);		
}
///////////////////////////////////////////////
//////////  customer_by_staff_report //////////
///////////////////////////////////////////////
public function customer_by_staff_report($limit=0){
	$data=array();
	$data['title'] = "Purchase Report";
    $data['section'] = "List";
     if($this->input->get('cust_names')) {
       $bid = $this->input->get('cust_names');
       $q="SELECT * FROM purchase_order WHERE customer like '%$bid%'";
    }else{// it show by default
        $q="SELECT * FROM `purchase_order` ORDER BY `date` DESC";
    }

    $this->load->library('pagination');
    $config['base_url'] = base_url('customer');
    $config['total_rows'] = $this->db->query($q)->num_rows();
    $config['per_page'] = 8; 
    $config["uri_segment"] = 2;
    $data['limit']=$limit;
    //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

    $data['customer']=$this->db->query("$q LIMIT $limit, ".$config['per_page'] )->result_array();
    $this->pagination->initialize($config);
    $data['link'] = $this->pagination->create_links();
	$this->load->view("customer_by_staff_report", $data);
}

////////////////////////////////////////////
//////////    genrate invoice     //////////
////////////////////////////////////////////
	// function for invoice id
	private function generateInvoice(){		
		$stamp = date("dmY");
		$mt = microtime();
		$mt = str_replace(" ", "", $mt);
		$mt = str_replace(".", "", $mt);
		$mt = substr($mt,0,6);		
		return $stamp.$mt;
	} 
////////////////////////////////////////////
//////////  genrate bil_invoice   //////////
////////////////////////////////////////////
	public function bil_invoice(){
		$data=array();
		if($this->input->get('id') && $this->input->get('id') != "" ){
			$inv_id = $this->input->get('id');
			$data['pur_order']=$query_data = $this->db->query("SELECT * FROM `purchase_order` WHERE `invoice_id`='$inv_id'")->row_array();
			$data['pur_pro']=$this->db->query("SELECT C.*, D.name FROM purchase_product as C left join products as D ON C.product_id=D.id WHERE invoice_id ='$inv_id'")->result_array();
		
			$num_query_data = count($query_data) ;
			if($num_query_data > 0){
				$customer = $query_data['customer'];
				$tax = $query_data['tax'];
				$payable_amt = $query_data['payable_amt'];
				$g_total = $payable_amt + $tax;
				$data['g_total'] = number_format((float) $g_total,2,'.','');
				$query2 = $this->db->query("SELECT * FROM `customers` WHERE `name`='$customer'")->row_array();
				$data['sum_amount'] = $this->db->query("SELECT SUM(total_amount) as t_amount from purchase_product where `invoice_id`= '$inv_id'")->row_array();			
			}else{
				$this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>'Wrong invoice id: Please try again !'));
				redirect(base_url()."selling");
			}		
		}
		$data['all_product']=$this->db->query("SELECT * FROM `purchase_order` ORDER BY id desc")->result_array();
		$data['setting'] = $this->db->query("select * from setting")->row_array();
		$this->load->view("bil_invoice",$data);
	}
//////////////////////////////////////////////
//////////// staff change password  //////////
//////////////////////////////////////////////
	public function staff_change_password(){
		$data='';
    $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
    $this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[re_password]');
    $this->form_validation->set_rules('re_password', 'Retype Password', 'required|trim');
    if($this->form_validation->run() == TRUE){
        $sessionData = $this->session->userdata('user_data');
        $this->data['id'] = $id = $sessionData['id'];
        $this->data['name'] = $sessionData['name'];
        $password = $this->input->post('newpassword');
        $old_password = $this->input->post('old_password');
        $sql=$this->db->get_where("user", array("id"=>$id,"password"=>$old_password) )->result_array();
        if(count($sql)>0){
            $this->db->where('id', $id);
            $s=$this->db->update("user",array('password'=>$password));
            if(count($s)>0){
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Password Successfully Change'));}
            redirect('staff_cp');
        }else{
               $data['error_msg'] = "Old  Passsword did not match";
             }
    }
		$this->load->view('staff_change_password',$data);
	}
////////////////////////////////////////////
////////   PERCHASE REPORT  ////////////////
/////////////////////////////////////////////
public function customer_perchase_report($limit=0){
	$data['title'] = "Supplier";
    $data['section'] = "List";
        if($this->input->get('membername')){ // when search by the member name
        $mid=$this->input->get('membername');
        $q="SELECT * FROM `members` Where `id`='$mid'";
    }else{// it show by default
        $q="SELECT * FROM `purchase_order` ORDER BY `date` DESC";
    }

    $this->load->library('pagination');
    $config['base_url'] = base_url('customer');
    $config['total_rows'] = $this->db->query($q)->num_rows();
    $config['per_page'] = 10; 
    $config["uri_segment"] = 2;
    $data['limit']=$limit;
    //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

    $data['customer']=$this->db->query("$q LIMIT $limit, ".$config['per_page'] )->result_array();
    $this->pagination->initialize($config);
    $data['link'] = $this->pagination->create_links();
    $this->load->view('customer_perchase_report',$data);
}
} ?>