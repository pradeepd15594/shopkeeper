<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
   
	function __construct() {
		parent::__construct();
	}
// index admin login procss page
      public function index($message = NULL)
    {
      if($this->session->userdata('user_data')!=null)
        {
           redirect(base_url()."admin_area");
        }
		$this->load->library('form_validation');
		$this->load->model('login_model');
		$data = array();
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run() == true){
			$result = $this->login_model->validate();
				if($this->session->userdata('user_data')!=null){
				$resultss =$this->session->userdata('user_data');
				if(count($resultss)>0 && $resultss['user_type'] == "admin"){
					redirect(base_url()."admin_area");
				}elseif(count($resultss)>0 && $resultss['user_type'] == "staff"){
	                    redirect(base_url()."staff_area");
	            }
            }else{
				$data['message'] = 'Login credantial incorrect! Try again';
			}
		}  
      $this->load->view('index', $data);
    } 
    public function logout(){
        $this->session->sess_destroy();
        redirect("login");
    }
   // End the admin index page login 
}
    ?>