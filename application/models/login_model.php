<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function validate(){
        // grab user input
        $name = $this->security->xss_clean($this->input->post('email'));
        $password = $this->security->xss_clean($this->input->post('password'));
        
        // Prep the query
        $this->db->where('user_name', $name);
        $this->db->where('password', $password);
        // Run the query
        $query = $this->db->get('user');
        // Let's check if there are any results
        //echo $this->db->last_query();
        if($query->num_rows == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();
            $data['user_data'] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'user_name' => $row->user_name,
                    'user_type' => $row->user_type,
                    'validated' => true
                    );
            $this->session->set_userdata($data);
            return true;
        }
        // If the previous process did not validate
        // then return false.
        return false;
        
        function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('home', 'refresh');
    }
    }
}
?>