<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/



$route['default_controller'] = "login";

//Admin Area
$route['dashboard'] = "admin_area/dashboard"; 
$route['products'] = "admin_area/product";
$route['key-change'] = "admin_area/change_password";
$route['add_user'] = "admin_area/add_user";
$route['stock'] = "admin_area/stock";
$route['add_member'] = "admin_area/member_add";
$route['member_list'] = "admin_area/member_list";
$route['member_list/(:any)'] = "admin_area/member_list/$1";
$route['member-accounts'] = "admin_area/members_accounts";
$route['member-accounts/(:any)'] = "admin_area/members_accounts/$1";
$route['member-accounts/(:any)/(:any)'] = "admin_area/members_accounts/$2";
$route['p-report'] = "admin_area/product_report";
$route['perchase-report'] = "admin_area/perchase_report";
$route['perchase-report/(:any)'] = "admin_area/perchase_report/$1";
$route['noti_circular'] = "admin_area/notification_circuler";
$route['view_contact'] = "admin_area/contact_form";
$route['setting'] = "admin_area/setting";
$route['customer'] = "admin_area/customer_perchase_report";
$route['customer/(:any)'] = "admin_area/customer_perchase_report/$1";
$route['pays_amount'] = "admin_area/pays_amount";
$route['sale_report'] = "admin_area/sales_report";
$route['sale_report/(:any)'] = "admin_area/sales_report/$1";
$route['sale_report/(:any)/(:any)'] = "admin_area/sales_report/$2";
$route['batch_no'] = "admin_area/batch_no";
$route['category'] = "admin_area/category";
$route['category/(:any)'] = "admin_area/category/$1";
// Main Pages Ends //
//staff area...
$route['staff_area'] = "staff_area";
$route['selling'] = "staff_area/selling";
$route['billing'] = "staff_area/bil_invoice";

$route['staff_cp'] = "staff_area/staff_change_password";
$route['cbsr'] = "staff_area/customer_by_staff_report";
$route['cbsr/(:any)'] = "staff_area/customer_by_staff_report/$1";

$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */