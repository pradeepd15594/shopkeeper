
// email box closing and open//
$(document).ready(function() {
    $(".target").click(function(){
        // $(".compose").toggle(500,'linear');
        $(".compose").hide('slow');
        $(".non_compose").show('slow');
    });
    $(".targetcompose").click(function(){
        $(".compose").show('slow');
        $(".non_compose").hide('slow');

    });
    $(".min").click(function(){
        $('.compose').animate({height: '36'}, 500);
        $('.min1').show();
        $('.min').hide();
        $('.compose').css({bottom:0});
    });
    $(".min1").click(function(){
        $('.compose').animate({height: '500'}, 500);
        $('.min1').hide();
        $('.min').show();
        $('.compose').css({bottom:60});
    });

    $("#search").on('keyup', function(){
        var searchVal = $(this).val();
        if(searchVal!=""){
            $("#searchContainer").find('a').each(function(){
                console.log($(this).attr("data-name"));
                 var searchResult= $(this).attr("data-name").toLowerCase().indexOf(searchVal.toLowerCase());
                 if(searchResult < 0){
                    $(this).fadeOut();
                 }else{
                    $(this).fadeIn();
                 }
            });
        }else{
            $("#searchContainer").find('a').fadeIn();
        }
    });
}); //Document.ready function are close here

/*
*  on change function
*/

$(function() {
    $('#talentid').on('change', function(){
        var talentid = $(this).val();
        var a = "";
        $.each(talentData, function(k,Val){
            if(k==talentid){
                a = Val;
            }

        })
        var HTML="";
        $.each(a, function(x, val){
            HTML+= "<div><input type='checkbox' id='test"+val['id']+"' value='"+val['id']+"' name='talenttype[]' /><label for='test"+val['id']+"'>"+val['name']+"</label></div>";
        })
        $('#talent-type').html(HTML);
    });
});

//input type only numeric, alphanumeric, onlynumeric  function start here
function onlyalphabates(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/[^a-zA-Z .]+/g, '');
}
function onlynumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^ 0-9]+/g, '');
 // inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}

function onlyfloatnumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^ 0-9.]+/g, '');
 // inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}
function alphanumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}
//input type only numeric, alphanumeric, onlynumeric function are end here

// });
