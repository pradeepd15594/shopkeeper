
$(function() {
    $( ".datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
        $("#e_status").on('change',function(){ 
            var stat = $(this).val();
                switch(stat){
                case '0':
                    $('#due_date').attr('disabled',true);
                        break;
                case '1':
                    $('#due_date').attr('disabled',false);
                        break;
                        default:
                        break;
                    }
        });
});

//input type only numeric, alphanumeric, onlynumeric  function start here
function onlyalphabates(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/[^a-zA-Z .]+/g, '');
}
function onlynumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^ 0-9.]+/g, '');
 // inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}

function onlyfloatnumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^ 0-9.]+/g, '');
 // inputtxt.value = inputtxt.value.replace(/\s/g, '').replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}
function alphanumeric(inputtxt) {
 inputtxt.value = inputtxt.value.replace(/[^a-zA-Z 0-9\n\r.]+/g, '');
}
//input type only numeric, alphanumeric, onlynumeric function are end here

/////////////////////////////////////////////////////////
////////////////   payment mode options  ////////////////
/////////////////////////////////////////////////////////
    $("#pay_mode").change(function() {      
        var m_val = $(this).val();
        if (m_val == 'Cash') {          
            $('#mcheque').hide();
            $('#mneft').hide();
            $('#mrtgs').hide();
        } else if (m_val == 'Cheque') {         
            $('#mcheque').show();
            $('#mneft').hide();
            $('#mrtgs').hide();
        } else if (m_val == 'NEFT') {           
            $('#mneft').show();
            $('#mcheque').hide();
            $('#mrtgs').hide();
        } else if (m_val == 'RTGS') {           
            $('#mrtgs').show();
            $('#mcheque').hide();
            $('#mneft').hide();
        } else {
            $('#mcheque').hide();
            $('#mneft').hide();
            $('#mrtgs').hide();
        }
    });
