$(document).ready(function() {
	$(document).on('click','.product-list button',function(){
		
		var name = $(this).attr('data-name');
		var code = $(this).attr('data-code');
		var id = $(this).attr('data-id');
		var value = $(this).val();
		var has_qty = $(this).attr('data-qty');
		
		var saleHTML = '<tr id="prid-'+id+'">'+
			'<td style="width: 9%;" class="text-center">'+
				'<button type="button" class="del_item tip" id="del-'+id+'" value="'+value+'" data-placement="right" data-original-title="Remove"><i class="fa fa-times-circle-o"></i></button>'+
			'</td>'+
			'<td style="width: 53%;">'+
				'<input type="hidden" class="code" name="product[]" value="'+name+'" id="product-'+id+'">'+
				'<input type="hidden" name="code[]" value="'+code+'" id="product-'+id+'">'+
				'<button type="button" class="btn btn-warning btn-block prod_name btn-xs text-left tip" data-placement="bottom" data-original-title="Click to increase quantity" data-name="'+name+'" data-has="'+has_qty+'" data-vatval="'+has_vat+'" data-stval="'+has_stax+'" id="'+id+'">'+name+' @ '+value+'</button>'+
			'</td>'+
			'<td style="width: 12%;" class="text-center">'+
				'<input class="keyboard nkb-input" name="quantity[]" type="hidden" value="1" id="quantity-'+id+'" onclick="this.select();" autocomplete="off">'+
				'<span id="quantity-view-'+id+'">1</span>'+
			'</td>'+
			'<td style="width: 24%;" class="text-right">'+
				'<input type="hidden" name="vt[]" value="'+has_vat+'" id="vt-'+id+'">'+
				'<input type="hidden" name="st[]" value="'+has_stax+'" id="st-'+id+'">'+
				'<input type="hidden" class="price" name="price[]" value="'+value+'" id="price-'+id+'">'+'<span id="price_'+id+'">'+value+'</span>'+
			'</td>'+
		'</tr>';
		
		if($('#prid-'+id).length==0){			
			TotalItems++;
			UpdateRates(value, has_vat, has_stax);
			ApplyDiscount();
			$("#count").text(TotalItems);
			$('#saletbl').append(saleHTML);
			$('.tip').tooltip();			
		}else{
			bootbox.alert("Product already added!");
		}
	});
	
	// $("#vat").on('click', function(){
		// UpdateRates(0);
		// ApplyDiscount();
	// });
	// $("#st").on('click', function(){
		// UpdateRates(0);
		// ApplyDiscount();
	// });
	
	// function UpdateRates(value){
	function UpdateRates(value, has_vat, has_stax){		
			TotalWithTax += parseFloat(value);
			// TotalVat += parseFloat(has_vat); 
			// TotalST += parseFloat(has_stax); 
			
			// ExcVat = (parseFloat(TotalWithTax * VatValue))/100;
			// ExcST = (parseFloat(TotalWithTax * StValue))/100;
			
			ExcVat += parseFloat(has_vat);			
			ExcST += parseFloat(has_stax);
						
			RawTotal = parseFloat(TotalWithTax) - parseFloat(ExcVat);			
			RawTotal = parseFloat(RawTotal) - parseFloat(ExcST);
			
			// if VAT is Checked
			// if($("#vat").prop('checked') == true){
				// RawTotal = parseFloat(TotalWithTax) - parseFloat(ExcVat);
				// $("#ts_con").text(ExcVat.toFixed(2));
				// $("#tax_val").val(VatValue+"%");
			// }else{
				// RawTotal = parseFloat(TotalWithTax);
				// $("#ts_con").text('0.00');
				// $("#tax_val").val(0+"%");
			// }			
			// if Service TAX is Checked
			// if($("#st").prop('checked') == true){				
				// RawTotal = parseFloat(TotalWithTax) - parseFloat(ExcST);
				// RawTotal = parseFloat(RawTotal) - parseFloat(ExcST);
				// $("#st_con").text(ExcST.toFixed(2));
				// $("#st_value").val(StValue+"%");
			// }else{
				// RawTotal = parseFloat(RawTotal);
				// $("#st_con").text('0.00');
				// $("#st_value").val(0+"%");				
			// }
						
			$("#ts_con").text(ExcVat.toFixed(2));
			$("#st_con").text(ExcST.toFixed(2));
			$("#tax_val").val(ExcVat.toFixed(2));
			$("#st_value").val(ExcST.toFixed(2));
			$("#total").text((RawTotal == 0) ? TotalWithTax.toFixed(2) : RawTotal.toFixed(2));
			$("#grand-total").text(TotalWithTax.toFixed(2));
			$("#total-payable").text(TotalWithTax.toFixed(2));			
	}
	
	function ApplyDiscount(){
		if(TotalWithTax == 0){return false;}
		DSValue = parseFloat(TotalWithTax * AppliedDiscountRate)/100;
		DiscountedValue = parseFloat(TotalWithTax - DSValue);
		$("#ds_con").text(DSValue.toFixed(2));
		$("#total-payable").text(DiscountedValue.toFixed(2));
		$("#discountRate").text("("+AppliedDiscountRate+"%)");
		$("#discount_val").val(AppliedDiscountRate+"%");		
	}

	function UpdateQuantity(NewQuantity,OldQuantity,value, has_vat, has_stax){					
			UpdateRates(value, has_vat, has_stax);
			ApplyDiscount();
	}
	
	$("#saletbl").on("click", '.prod_name', function(){
        var rw = $(this).attr('id');
        var pn = $(this).attr('data-name');			
		var hq = $(this).attr('data-has');
		var h_vat = $(this).attr('data-vatval');
		var h_stax = $(this).attr('data-stval');
		
		$('#proModalLabel').text(pn+' @ '+$('#price-' + rw).val());
        $('#rwNo').val(rw);			
        $('#ava_qty').val(hq);			
        $('#h_vat').val(h_vat);			
        $('#h_stax').val(h_stax);			
        $('#oPrice').val($('#price-' + rw).val());
        $('#oQuantity').val($('#quantity-' + rw).val());
        $('#nPrice').val($('#price-' + rw).val());		
        $('#nQuantity').val($('#quantity-' + rw).val());
        $('#proModal').modal();
        return false
    });
	
	$("#update-row").click(function(){
        var rw = $('#rwNo').val();
        var hq = $('#ava_qty').val();
        var hv = $('#h_vat').val();
        var hs = $('#h_stax').val();
		
        var op = parseFloat($('#price-' + rw).val());
        var oq = parseInt($('#quantity-' + rw).val());
        var np = parseFloat($('#nPrice').val());
		
		if(parseInt($('#nQuantity').val())>hq){			
			bootbox.alert("Sorry, this product has only "+hq+" quantities remaining!");
		}else{		
			var nq = parseInt($('#nQuantity').val());			
			var row_price = op;
			var row_quantity = oq;
			var priceToSend = parseFloat(row_price*(nq - oq));
			var nvat = parseFloat(hv*(nq - oq));
			var nst = parseFloat(hs*(nq - oq));
			
			// Update only if qty increase
			// if(nq>oq){
				// UpdateQuantity(nq, oq, priceToSend, nvat, nst);
			// }
			
			UpdateQuantity(nq, oq, priceToSend, nvat, nst);
			
			$('#price-' + rw).val(np);
			$('#quantity-' + rw).val(nq);
			$('#quantity-view-' + rw).text(nq);
			
			if ($('#proModalLabel').text().length > 13) {
				pName = $('#proModalLabel').text().substring(0, 13) + '..'
			} else {
				pName = $('#proModalLabel').text()
			}
			
			TotalItems = TotalItems + nq - row_quantity;
			$("#count").text(TotalItems);
			$('#price_' + rw).text((np * nq).toFixed(2));
			$('#proModal').modal('hide');
			return false
		}
    });
	
	$("#add_discount").click(function() {
		if(TotalWithTax == 0){bootbox.alert("Please add products first!");return false;}
        var dis_opt = '';
		$.each(DiscountValueArray,function(key, value){
			if(AppliedDiscountRate==value){
				var sel = "selected";
			}else{
				var sel = '';
			}
			dis_opt+="<option value='"+ value +"' "+ sel +" >"+ value +"%</option>";
		})
        bootbox.dialog({            
            message: "<select class='form-control input-sm' id='get_ds'>"+ dis_opt +"</select>",
            title: "Apply Discount (for eg: 5%)",
            buttons: {
                main: {
                    label: "Update",
                    className: "btn-primary btn-sm",
                    callback: function() {
                        var ds = $('#get_ds').val();
                        AppliedDiscountRate = ds;
						ApplyDiscount();						
					}
                },
				closeButton: {
                    label: "Cancel",
					className: "btn-default btn-sm",
				}
            }
        });
        return false
    });
	
    $("#saletbl").on("click", 'button[class^="del_item tip"]', function() {
				
		var delID = $(this).attr('id');
        var dl_id = delID.split("-");
        var rw_no = dl_id[1];
        var p1 = $('#price-' + rw_no);
        var q1 = $('#quantity-' + rw_no);
		
        var vt1 = $('#vt-' + rw_no);
        var st1 = $('#st-' + rw_no);
		
        var row_quantity = parseInt(q1.val());
        var row_price = parseFloat(p1.val() * q1.val());		
        var row_vat = parseFloat(vt1.val() * q1.val());
        var row_stax = parseFloat(st1.val() * q1.val());
		
		TotalWithTax = TotalWithTax - row_price; // total price
        TotalItems = TotalItems - row_quantity;
		
		// UpdateRates(0, 0, 0);
		ApplyDiscount();
		
		ExcVat = ExcVat - row_vat;		
		ExcST = ExcST - row_stax;
				
		current = TotalWithTax - ExcVat - ExcST;
		current = parseFloat(current).toFixed(2);
		
        grand_total = parseFloat(TotalWithTax).toFixed(2);
		
		$("#ts_con").text(ExcVat.toFixed(2));
		$("#st_con").text(ExcST.toFixed(2));
		
		$("#tax_val").val(ExcVat.toFixed(2));
		$("#st_value").val(ExcST.toFixed(2));
		
        $("#total").text(current);
        $("#grand-total").text(grand_total);
        $("#total-payable").text(grand_total);
        $("#count").text(TotalItems);       
        row_id = $("#prid-" + rw_no);
        row_id.remove()
    });
	
	$("#cancel").click(function() {
        bootbox.confirm("Are you sure to cancel the sale?", function(result) {
            if (result == true) {
                $("#saletbl").empty();
                count = 1, total = 0, tax_value = 0, an = 1;
                $("#ds_con").text('0.00');
                $('#ts_con').text('0.00');
                $('#st_con').text('0.00');
                $("#grand-total").text('0.00');
                $("#total-payable").text('0.00');
                $("#total").text('0.00');
                $("#count").text(TotalItems);				
				window.location.href=base;
            }
        })		
    });
	
    $("#payment").click(function() {		
        $("#pay").empty();
		$('#inv_type').val('retail');
		$('#customer').val('');			
		$('#customer_phone').val('');
		$('#customer_address').val('');
		
		var gt = $("#grand-total").text();		
		var vtx = parseFloat($("#ts_con").text());
		var stx = parseFloat($("#st_con").text());
		var dsv = parseFloat($('#ds_con').text());
		
		// gt = gt - parseFloat($('#ds_con').text());
		gt = gt - dsv;
		
		twt = parseFloat(gt).toFixed(2);		
		
        if (isNaN(twt) || twt == '0.00') {
            bootbox.alert('Please add product to sale first');            
            return false;
        }
		
        twt = parseFloat(twt).toFixed(2);
		
        $('#twt').text(twt);
        $('#fcount').text(TotalItems);
        $('#payModal').modal();
        $('#total_item').val(TotalItems);
        $('#cust_name').on("change blur click", function() {
            $('#customer').val($(this).val());			
			$('#customer_phone').val($('#cust_phone').val());
			$('#customer_address').val($('#cust_address').val());
        });
		$('#cust_phone').on("change blur click", function() {
            $('#customer_phone').val($(this).val());
        });
		$('#cust_address').on("change blur click", function() {
            $('#customer_address').val($(this).val());
        });
        $('#paid-amount').change(function() {
            $('#paid_val').val($(this).val())
        });
        $('#pcc').change(function() {
            $('#cc_no_val').val($(this).val())
        });
        $('#pcc_holder').change(function() {
            $('#cc_holder_val').val($(this).val())
        });
        $('#cheque_no').change(function() {
            $('#cheque_no_val').val($(this).val())
        });
        $("#paid_by").change(function() {
            var p_val = $(this).val();
            $('#rpaidby').val(p_val);
            if (p_val == 'cash') {
                $('.pcheque').hide();
                $('.pcc').hide();
                $('.pcash').show();
                $('input[id^="paid-amount"]').keydown(function(e) {					
                    paid = parseFloat($(this).val());
                    if (e.keyCode == 13) {
                        if (paid < total) {
                            bootbox.alert('Paid amount is less than payable amount');
                            return false
                        }
						
                        $("#balance").empty();						
                        var balance = paid - twt;
                        balance = parseFloat(balance).toFixed(2);
                        $("#balance").append(balance);
                        e.preventDefault();
                        return false
                    }
                })
            } else if (p_val == 'CC') {
                $('.pcheque').hide();
                $('.pcash').hide();
                $('.pcc').show()
            } else if (p_val == 'Cheque') {
                $('.pcc').hide();
                $('.pcash').hide();
                $('.pcheque').show()
            } else {
                $('.pcheque').hide();
                $('.pcc').hide();
                $('.pcash').hide()
            }
        });
		
		$("#invoice_type").change(function() {
            var i_val = $(this).val();
            $('#inv_type').val(i_val);
		});
		
        if (KB == 1) {
            $('#paid-amount, kb-input').keyboard({
                restrictInput: true,
                preventPaste: true,
                autoAccept: true,
                alwaysOpen: false,
                openOn: 'click',
                layout: 'costom',
                display: {
                    'a': '\u2714:Accept (Shift-Enter)',
                    'accept': 'Accept:Accept (Shift-Enter)',
                    'b': '\u2190:Backspace',
                    'bksp': 'Bksp:Backspace',
                    'c': '\u2716:Cancel (Esc)',
                    'cancel': 'Cancel:Cancel (Esc)',
                    'clear': 'C:Clear'
                },
                position: {
                    of: null,
                    my: 'center top',
                    at: 'center top',
                    at2: 'center bottom'
                },
                usePreview: false,
                customLayout: {
                    'default': ['1 2 3 {clear}', '4 5 6 .', '7 8 9 0', '{accept} {cancel}']
                },
                beforeClose: function(e, keyboard, el, accepted) {
                    var paid = parseFloat(el.value);
                    if (paid < twt) {
                        bootbox.alert('Paid amount is less than payable amount');
                        $(this).val('');
                        return false
                    }
                    $("#balance").empty();
                    var balance = paid - twt;
                    balance = parseFloat(balance).toFixed(2);
                    $("#balance").append(balance)
                }
            })
        } else {
            $("#payModal").on("change", '#paid-amount', function() {
                var paid = parseFloat($(this).val());
                if (paid < twt) {
                    bootbox.alert('Paid amount is less than payable amount');
                    $(this).val('');
                    return false
                }
                $("#balance").empty();
                var balance = paid - twt;
                balance = parseFloat(balance).toFixed(2);
                $("#balance").append(balance)
            })
        }
        $('#submit-sale').click(function() {
			if($('#cust_name').val()==''){
				$('#cust_name').attr('style','border: red solid 1px;');
				bootbox.alert('Customer name can not be blank !');				
				return false;
			}
            $('#submit').trigger('click')
        })
    });
	
	// estimate modal
	$("#estimate").click(function() {
		$('#customer').val('');			
		$('#customer_phone').val('');
		$('#customer_address').val('');
		
		var egt = $("#grand-total").text();
		
		egt = (egt + parseFloat($('#ts_con').text())) - parseFloat($('#ds_con').text());
		etwt = parseFloat(egt).toFixed(2);		
        
		
        if (isNaN(etwt) || etwt == '0.00') {
            bootbox.alert('Please add product to sale first');            
            return false
        }
        etwt = parseFloat(etwt).toFixed(2);

		$('#inv_type').val('estimate');
        $('#etwt').text(etwt);
        $('#efcount').text(TotalItems);
        $('#estimModal').modal();
        $('#total_item').val(TotalItems);
		
        // $('#ecust_name').on("change blur keyup input focus textInput propertychange paste click", function() {
        $('#ecust_name').on("change blur click", function() {
            $('#customer').val($(this).val());
			$('#customer_phone').val($('#ecust_phone').val());
			$('#customer_address').val($('#ecust_address').val());
        });
		// $('#ecust_phone').on("change blur keyup input focus textInput propertychange paste click", function() {
		$('#ecust_phone').on("change blur click", function() {
            $('#customer_phone').val($(this).val());
        });
		$('#ecust_address').on("change blur click", function() {
            $('#customer_address').val($(this).val());
        });
        
        $('#submit-estimate').click(function() {
			if($('#ecust_name').val()==''){
				$('#ecust_name').attr('style','border: red solid 1px;');
				bootbox.alert('Customer name can not be blank !');				
				return false;
			}
            $('#submit').trigger('click')
        });
    });
	
	// to hold bill
    $('#hold').click(function() {
		
		if (count <= 1) {
            bootbox.alert('Plesae add product before saving bill');
            return false
        }
		
        suspend = $('<span></span>');
        suspend.html('<input type="hidden" name="suspend" value="yes" />');
        suspend.appendTo("#hidesuspend");
        $('#total_item').val(count);
        $('#susModal').modal();
        $('.pcustomer').change(function() {
            $('#customer').val($(this).val())
        });
        $('#hold_ref_v').change(function() {
            $('#hold_ref').val($(this).val())
        });
        $('#submit-hold').click(function() {
            if ($('#hold_ref_v').val() != '') {
                $('#submit').trigger('click')
            } else {
                bootbox.alert('Please type the hold bill referebce');
                return false
            }
        })
    });
	
	// to add customer
	$("#add-customer").click(function() {
        var newCustomer = new Array();
        newCustomer[0] = $('#cname').val();
        newCustomer[1] = $('#cemail').val();
        newCustomer[2] = $('#cphone').val();
        newCustomer[3] = $('#cf1').val();
        newCustomer[4] = $('#cf2').val();
        $.ajax({
            type: "post",
            async: false,
            url: "",
            data: {
                csrf_pos: "",
                data: newCustomer
            },
            dataType: "json",
            success: function(data) {
                result = data.msg;
                cu = data.c;
                $('#customer').val(data.cid);
            },
            error: function() {
                bootbox.alert('<em>customer_request_failed</em>');
                result = false;
                return false
            }
        });
        if (result == 'Customer Successfully Added') {
            $('.inv_cus_con').html(cu);
            $('#customerModal').modal('hide');
            $('#cname').val('');
            $('#cemail').val('');
            $('#cPhone').val('');
            $('#cf1').val('');
            $('#cf2').val('');
            bootbox.alert('Customer Successfully Added')
        } else {
            var error = $('<div class=\"alert alert-danger\"></div>');
            error.html("<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" + result);
            error.appendTo("#customerError");
            return false
        }
    });
	
    $("#payModal").on('click', '.showCModal', function() {
        $('#payModal').modal('hide');
        $('#show_m').val('pay');
        $('#customerModal').modal('show');
        return false
    });
	
    $("#susModal").on('click', '.showCModal', function() {
        $('#susModal').modal('hide');
        $('#show_m').val('sus');
        $('#customerModal').modal('show');
        return false
    });
	
    $('#customerModal').on('hide.bs.modal', function() {
        if ($('#show_m').val() == 'pay') {
            $('#payModal').modal('show')
        }
        if ($('#show_m').val() == 'sus') {
            $('#susModal').modal('show')
        }
    });
	
	// tooltip
	$('.tip').tooltip();
	
	// radio checked
	$(".usertype").click(function(){		
		var current = $('.usertype:checked').val();
		if(current=='Staff'){
			$('#store').fadeIn();
		}else{$('#store').fadeOut();}
	});
	
	// calendar
	$("#start_date").datepicker({		
		format: "yyyy-mm-dd",
		autoclose: true
	});
	$("#end_date").datepicker({		
		format: "yyyy-mm-dd",
		autoclose: true		
	});
	$("#due_date").datepicker({		
		format: "dd MM yyyy",
		autoclose: true		
	});
	
	// form
	$('#form').hide();       
	$(".toggle_form").slideDown('slow'); 
	$('.toggle_form').click(function(){
		$("#form").slideToggle();
		return false;
	});		
	
	// alert hide/show
	$('.alert').click(function(){ $(this).fadeTo(500, 0).slideUp(500, function(){ $(this).remove(); }); });
	window.setTimeout(function() { $(".alert").fadeTo(500, 0).slideUp(500, function(){ $(this).remove(); }); }, 5000);
	
	// current time function
	function curtime() {
		
        now = new Date();
        var month_names = new Array();
        month_names[month_names.length] = "January";
        month_names[month_names.length] = "February";
        month_names[month_names.length] = "March";
        month_names[month_names.length] = "April";
        month_names[month_names.length] = "May";
        month_names[month_names.length] = "June";
        month_names[month_names.length] = "July";
        month_names[month_names.length] = "August";
        month_names[month_names.length] = "September";
        month_names[month_names.length] = "October";
        month_names[month_names.length] = "November";
        month_names[month_names.length] = "December";
		
        var day_names = new Array();
        day_names[day_names.length] = "Sunday";
        day_names[day_names.length] = "Monday";
        day_names[day_names.length] = "Tuesday";
        day_names[day_names.length] = "Wednesday";
        day_names[day_names.length] = "Thursday";
        day_names[day_names.length] = "Friday";
        day_names[day_names.length] = "Saturday";
        hour = now.getHours();
        min = now.getMinutes();
        sec = now.getSeconds();
        if (min <= 9) {
            min = "0" + min
        }
        if (sec <= 9) {
            sec = "0" + sec
        }
        if (hour > 12) {
            hour = hour - 12;
            add = "PM"
        } else {
            hour = hour;
            add = "AM"
        }
        if (hour == 12) {
            add = "PM"
        }
		var time = '';
        time = day_names[now.getDay()] + ", " + now.getDate() + " " + month_names[now.getMonth()] + " " + now.getFullYear() + ", " + ((hour <= 9) ? "0" + hour : hour) + ":" + min + ":" + sec + " " + add;
        if (document.getElementById('cur-time')) {
            document.getElementById('cur-time').innerHTML = time;
        } else if (document.layers) {
            document.layers.theTime.document.write(time);
            document.layers.theTime.document.close();
        }
        setTimeout(function(){
			curtime();
		}, 1000)
    }
    window.onload = curtime();
});

// function for printing page
function PrintDiv() {	
	var pageTitle = document.getElementById('pageTitle');
	var titleOfPage = pageTitle.innerHTML;
	var divToPrint = document.getElementById('printable');		
	var popupWin = window.open('', '_blank', 'width=768,height=650,location=no,left=200px');
	popupWin.document.open();
	popupWin.document.write('<html><title>' + titleOfPage + '</title><link rel="stylesheet" type="text/css" href="dist/css/metro-bootstrap.min.css"><link rel="stylesheet" type="text/css" href="styles/style.css"><link rel="stylesheet" type="text/css" href="styles/print.css" media="screen"/><style media="print">.no-print{ display: none; }.btn{ display: none; }.printHeading{ display: block !important; }</style></head><body>')
	popupWin.document.write(divToPrint.innerHTML);
	popupWin.document.write('<p class="text-center"><br /><button class="btn btn-info" onClick="window.print();">Print</button> <button class="btn btn-danger" onClick="window.close();">Close</button></p></body></html>');
	popupWin.document.close();		
}

// input accept only numeric value
function isNumber(evt) {
	evt = (evt) ? (evt) : window.event;
	var charcode = (evt.which) ? evt.which : evt.keycode;	
	if(charcode > 31 && (charcode < 48 || charcode > 57)){
		bootbox.alert("Accept only Numeric value!");		
		return false;
	}	
	return true;
}
// input accept only alphabetic values
function alpha(e) {
	var k;
	document.all ? k = e.keyCode : k = e.which;
	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}

$(function() {	
$(document).on('keydown.autocomplete', "#cust_name", function() {		
		$(this).autocomplete({
			minLength: 0,
			source: names,
			focus: function( event, ui ) {
				$( "#cust_name" ).val( ui.item.name );
				return false;
			},
			select: function( event, ui ) {
				$( "#cust_name" ).val( ui.item.name );        
				$( "#cust_phone" ).val( ui.item.phone );
				$( "#cust_address" ).val( ui.item.address );				
				$( "#paid_by" ).focus();
				return false;
			}
		},"appendTo", "#payModal").autocomplete( "instance" )._renderItem = function( ul, item ) {
		return $( "<li>" )
			.append( item.name + "<br />Phone: " + item.phone + "<br />Address: " + item.address )
			.appendTo( ul );
		};		
	});
	
	$(document).on('keydown.autocomplete', "#ecust_name", function() {		
		$(this).autocomplete({
			minLength: 0,
			source: names,
			focus: function( event, ui ) {
				$( "#ecust_name" ).val( ui.item.name );
				return false;
			},
			select: function( event, ui ) {
				$( "#ecust_name" ).val( ui.item.name );        
				$( "#ecust_phone" ).val( ui.item.phone );
				$( "#ecust_address" ).val( ui.item.address );
				$( "#submit-estimate" ).focus();
				return false;
			}
		},"appendTo", "#estimModal").autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( item.name + "<br />Phone: " + item.phone + "<br />Address: " + item.address )
			.appendTo( ul );
		};
	});
	$(document).on('keydown.autocomplete', "#estcust_name", function() {		
		$(this).autocomplete({
			minLength: 0,
			source: cnames,
			focus: function( event, ui ) {
				$( "#estcust_name" ).val( ui.item.name );
				return false;
			},
			select: function( event, ui ) {
				$( "#estcust_name" ).val( ui.item.name );        
				$( "#estcust_phone" ).val( ui.item.phone );
				$( "#estcust_address" ).val( ui.item.address );			
				return false;
			}
		})
		.autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( item.name + "<br />Phone: " + item.phone + "<br />Address: " + item.address )
			.appendTo( ul );
		};
	});
});